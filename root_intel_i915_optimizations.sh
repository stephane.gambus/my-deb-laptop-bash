#!/bin/bash
# root_intel_i915_optimizations.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo


if [ "$(lsmod |grep "^i915" |wc -l)" -eq "1" ]; then
   if [ -e /etc/modprobe.d/i915.conf ]; then
      echo "/etc/modprobe.d/i915.conf already present, so we do nothing" 
   else
      echo "options i915 modeset=1"       | tee -a  /etc/modprobe.d/i915.conf
      echo "options i915 fastboot=1"      | tee -a  /etc/modprobe.d/i915.conf     
      echo "options i915 enable_guc=3"    | tee -a  /etc/modprobe.d/i915.conf
   #   echo "options i915 enable_psr=1"    | tee -a  /etc/modprobe.d/i915.conf
   #   echo "options i915 enable_fbc=1"    | tee -a  /etc/modprobe.d/i915.conf
   #   echo "options i915 enable_gvt=1"    | tee -a  /etc/modprobe.d/i915.conf
   fi

else

   echo "This computer does not use i915 graphics card, so we do nothing"

fi



exit
