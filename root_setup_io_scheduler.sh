#!/bin/bash
################################################################################
# root_setup_io_scheduler.sh 
################################################################################
# setup IO scheduler
# for all the ssd devices, if mq-deadline io scheduler is supported ant not activated,   
# we activate it
# note: this script need sysfsutils package installed
################################################################################

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo


block_device_name=""
file_path=""
is_rotationnal=""
path_rotational=""
path_scheduler=""
string_rotational=""
string_scheduler=""

# while there is a block devices with type="disk",  do
lsblk -l | awk '{printf "%-40s %-6s\n", $1,$6}' | awk '$2=="disk"' | awk '{print $1}'  | while read ligne ; do

   block_device_name=$(echo $ligne | awk '{print $1}')
   path_rotational='/sys/block/'$block_device_name'/queue/rotational'
   path_scheduler='/sys/block/'$block_device_name'/queue/scheduler'
   if [ -e "$path_rotational" ]; then

      string_rotational=$(cat $path_rotational)
      if [ "$string_rotational" = 0 ]; then

         if [ -e "$path_scheduler" ]; then
             string_scheduler=$(cat $path_scheduler)

	     # If the idefault scheduler is not [mq-deadline] and  mq-deadline  is selectable
	     if [ "$(echo $string_scheduler |grep '\[mq-deadline\]' |wc -l)" = 0 ] && [ "$(echo $string_scheduler |grep 'mq-deadline' |wc -l)" = 1 ]; then

		if [ "$(cat /etc/sysfs.conf |grep '/queue/scheduler = mq-deadline' |wc -l)" = 0 ]; then

		   # add a line in /etc/sysfs.conf to setup the default scheduler to [mq-deadline]       
		   echo 'block/'$block_device_name'/queue/scheduler = mq-deadline' | tee -a /etc/sysfs.conf

                else
                   echo "the configuration line in /etc/sysfs.conf is already present"
                fi	

             else
		echo "nothing to do, with the scheduler..."
             fi

         fi

     else
         echo "this device is rotational, so we do not change the scheduler"
     fi
   else 
      echo $string_rotational" does not exist, so we do nothing..."
   fi

done


exit
