#!/bin/bash
################################################################################
# main program 
################################################################################


# Run in debug mode
#set -x

# stop on first error
set -e


# source libraries
source libs/all




################################################################################
#
################################################################################
run_prog () {

   set -e
   local user=$1           # user
   local prog=$2           # programm to run  
   local num_last_prog=$3  # number of the last programm
   local num_prog=$4       # number of the programme
   
   local val1=$((100 / $num_last_prog)) 
   local val_percent=$(($val1 * $num_prog))
   
   echo_for_whiptail "$val_percent" "$2"

   echo -e "\n\n\n------- Begin -------- : $2"                 >>./tmp/logfile 2>&1 
   sudo -H -u "$1"   bash -c "./$2                             >>./tmp/logfile 2>&1"
   echo -e "------- End   -------- : $2"                       >>./tmp/logfile 2>&1
   
   sleep 2


}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo

# get the user login
user=$(get_user)

# empty logfile
sudo -H -u $user  bash -c '>./tmp/logfile'

# check the internet connexion
check_internet_connexion

# apt setup and install
./root_setup_apt.sh
./root_install_apt_packages.sh

# flatpak setup and install
./root_setup_flatpack_remote_repository.sh
sudo -H -u $user  bash -c './user_install_flatpak_packages.sh'

# get the number of lines in scripts.list
num_last_prog=$(cat ./lists/scripts.list | wc -l)

{
   set -e

   i=0

   while read ligne ; do

      i=$(( "$i" + 1 )) 

      user_to_use=$(echo $ligne | awk -F _ '{print $1}')
      script_to_run=$(echo $ligne | awk '{print $1}')

      # if $user_to_use and $script_to_run ar not empty
      if [ ! -z "$user_to_use" ] && [ ! -z "$script_to_run" ]; then

         if [ "$user_to_use" == "root" ] || [ "$user_to_use" == "user" ]; then
           echo "user_to_use="$user_to_use"  script_to_run="$script_to_run

           if [ "$user_to_use" == "user" ]; then user_to_use=$user; fi

           run_prog "$user_to_use"  "$script_to_run" "$num_last_prog" "$i"  

         else
            echo "the script name format is incorrect in ./list: root_.... or user_..."
         fi

      else
         echo "the script name format is incorrect in ./list"
      fi

   done    <  ./lists/scripts.list




}   |   whiptail --gauge "Configuration " 6 60 0






exit 0
