#!/bin/bash
# root_setup_plymouth.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;




################################################################################
# setup plymouth theme name named in ./lists/plymouth-theme
################################################################################
setup_plymouth_theme_name () {

local i=0
local found=0


# if there is only one line in file, its ok
if [ "$(cat ./lists/plymouth-theme | wc -l)" -eq 1 ]; then

   # get the theme name 
   theme_name=$(awk 'NR==1 {print $1; exit}' lists/plymouth-theme)


   # check if the theme exist
   for i in `plymouth-set-default-theme -l| awk '{ print $1 }'`;do
      if [ "$i" == "$theme_name" ]; then found=1; fi
   done


   # if the theme if found
   if [ "$found" -eq 1 ]; then

      # set the theme
      /usr/sbin/plymouth-set-default-theme -R $theme_name 	    

   else
      echo "the theme $theme_name in ./lists/plymouth-theme is not present, so we do nothing" 
   fi 

else
   echo "the file ./lists/plymouth-theme need to have only one line containing a plymouth theme name"
fi


}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo


packagename="plymouth-themes"

# check if the package is installed
state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $packagename || true)

if [ "$state" == "installed" ]; then

  setup_plymouth_theme_name 

else
   echo "the package $packagename is not installed, so we do nothing"

fi



exit
