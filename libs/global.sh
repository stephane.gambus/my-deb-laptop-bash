#!/bin/bash


function run_step () {

	step_name="$1"
	shift
	cmd="${@}"
	echo "####### Running step :  $step_name"
	${@}
	ret=$?
	echo "###### Step ${step_name} finnished with ret code = $ret"
	echo ""
	
	return $ret
}


function run_step_multicmd () {

	
	step_name="$1"
	shift
	cmd="${@}"
	echo "####### Running step :  $step_name"

	while read line; do
		$line
		ret=$?
		if [ "$ret" -ne 0 ]; then
			echo "ERROR on command : $line"
			stop_on_error
		fi

	done
	echo "###### Step ${step_name} finnished with success"
	echo ""
	
	return $ret
}




################################################################################
# echo_result
################################################################################
echo_result () {

# args
local arg_01="$1"        
local arg_02="$2"

local arg_01_lenght=${#arg_01}        
local max_length_echo=100
local number_of_points=0
local point_string=""


# nuber of points calculus
let "number_of_points=max_length_echo - 7 - arg_01_lenght"
point_string=$(for i in `seq $number_of_points`;do  echo -n "." | tr -d ' ';done)

#------------------------------
# echo only KO in tty
#------------------------------

# set strings for OK and KO
local str_ok="\e[1;32mOK\e[0m"
local str_ko="\e[1;31mKO\e[0m"


if [ "$arg_02" == "1" ]; then result=$str_ok; else result=$str_ko; fi


# echo only KO 
if [ "$result" == "$str_ko" ]; then
  /bin/echo -e "\e[39m "$1" "$point_string"[ "$result" ]"
fi



#------------------------------
# echo in tmp/logfile
#------------------------------

if [ "$arg_02" == "1" ]; then result="OK"; else result="KO"; fi

/bin/echo -e "\e[39m "$1" "$point_string"[ "$result" ]" >> tmp/logfile


}


################################################################################
# echo_result_on console
################################################################################
echo_result_on_console () {

# args
local arg_01="$1"
local arg_02="$2"

local arg_01_lenght=${#arg_01}
local max_length_echo=100
local number_of_points=0
local point_string=""


# nuber of points calculus
let "number_of_points=max_length_echo - 7 - arg_01_lenght"
point_string=$(for i in `seq $number_of_points`;do  echo -n "." | tr -d ' ';done)

#------------------------------
# echo OK and KO in tty
#------------------------------

# set strings for OK and KO
local str_ok="\e[1;32mOK\e[0m"
local str_ko="\e[1;31mKO\e[0m"


if [ "$arg_02" == "1" ]; then result=$str_ok; else result=$str_ko; fi


/bin/echo -e "\e[39m "$1" "$point_string"[ "$result" ]"


}


################################################################################
# controls and exit if necessary (global checks)
################################################################################
controls () {



# if the distributor id is not "Debian", we exit
if [ "$(lsb_release -s -i)" != "Debian" ]; then
   echo "This is not a Debian Distribution, so we exit"
   exit 1
fi

# if the debian version codename is not "buster", we exit
#if [ "$(lsb_release -s -c)" != "buster" ]; then
#   echo "This script is for Debian version 10 buster only, so, we exit"
#   exit 2
#fi

# if the cpu architecture is not amd64, we exit
if [ "$(dpkg --print-architecture)" != "amd64" ]; then
   echo "This script is only for amd64 architecture, so, we exit"
   exit 3
fi


# get the number of users in the sudo group
local users_number_in_sudo_group=$(/usr/bin/getent group sudo | cut -d: -f4 | awk -F',' '{ print NF }')

# if there is no user in the sudo group, we exit 
if [ "$users_number_in_sudo_group" -eq "0" ]; then
   echo "this program need one user in the sudo group"
   exit 4
fi

# if there is too much user in the sudo group, we exit 
if [ "$users_number_in_sudo_group" -gt "1" ]; then
   echo "this program need only one user in the sudo group"
   exit 5
fi


}



################################################################################
# controls and exit if necessary + check if the program is run without sudo
################################################################################
controls_without_sudo () {


# check if the program is run without sudo
if [ "$(whoami)" == "root" ]; then
   echo "this program need need to be run as user (without sudo command)"
   exit 6
fi


# global checks
controls 



}


################################################################################
# controls and exit if necessary + check if the program is run with sudo
################################################################################
controls_with_sudo () {

# check if the program is run with sudo
if [ "$(whoami)" != "root" ]; then
   echo "this program need need to be run as root or with sudo"
   exit 6
fi


# global checks
controls 



}



################################################################################
# echo_for_whiptail
################################################################################
echo_for_whiptail () {
local number=$1
local text=$2

   echo "XXX"
   echo "$number"
   echo "$text"
   echo "XXX"

}



################################################################################
# get user name and put it in a file
################################################################################
function get_user () {

local path_file='./tmp/user'
local user=''

if [ -e "$path_file" ]; then
   user=$(cat "$path_file" | awk '{ print $1 }')
else
   user=$(/usr/bin/getent group sudo | cut -d: -f4)

   # write user name
   sudo -H -u $user  bash -c 'echo '$user' >'$path_file
fi


echo $user


if [ -z "$user" ]; then ret=1; else ret=0; fi

return $ret

}

