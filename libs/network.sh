#!/bin/bash


################################################################################
# Ping gateway to verify connectivity with LAN
################################################################################
function pinggateway
{

   echo "Pinging gateway ($GW) to check for LAN connectivity"                               >>./tmp/logfile 2>&1

   # check if the gateway exist
   if [ "$GW" = "" ]; then
      echo "There is no gateway. Probably disconnected..."                                  >>./tmp/logfile 2>&1
      nb_problem=$((nb_problem + 1)) 
      exit 1
   fi

   # ping the gateway
   ping $GW -c 4                                                                            >>./tmp/logfile 2>&1
   if [ $? -eq 0 ]; then
      echo "LAN Gateway pingable."                                                          >>./tmp/logfile 2>&1
   else 
      echo "LAN Gateway is not pingable."                                                   >>./tmp/logfile 2>&1
      nb_problem=$((nb_problem + 1)) 
   fi


}


################################################################################
#
################################################################################
function portscan
{
   echo "--------------------------------------------------------------------------------"  >>./tmp/logfile 2>&1
   echo "Starting port scan of $checkdomain port 80"                                        >>./tmp/logfile 2>&1
   if nc -zw1 $checkdomain  80; then
      echo "Port scan good, $checkdomain port 80 available"                                 >>./tmp/logfile 2>&1
   else
      echo "Port scan of $checkdomain port 80 failed."                                      >>./tmp/logfile 2>&1
      nb_problem=$((nb_problem + 1)) 
   fi
}



################################################################################
#
################################################################################
function pingnet
{
   echo "--------------------------------------------------------------------------------"  >>./tmp/logfile 2>&1
   echo "Pinging $checkdomain to check for internet connection."                            >>./tmp/logfile 2>&1
   ping $checkdomain -c 4                                                                   >>./tmp/logfile 2>&1

   if [ $? -eq 0 ]; then
      echo "$checkdomain pingable. Internet connection is most probably available."         >>./tmp/logfile 2>&1
   else
      echo "Could not establish internet connection. Something may be wrong here."          >>./tmp/logfile 2>&1
      nb_problem=$((nb_problem + 1)) 
   fi
}



################################################################################
#
################################################################################
function pingdns
{
   echo "--------------------------------------------------------------------------------"  >>./tmp/logfile 2>&1
   #Grab first DNS server from /etc/resolv.conf
   echo "Pinging first DNS server in resolv.conf ($checkdns) to check name resolution"      >>./tmp/logfile 2>&1
   ping $checkdns -c 4                                                                      >>./tmp/logfile 2>&1
   if [ $? -eq 0 ]; then
      echo "$checkdns pingable. Proceeding with domain check."                              >>./tmp/logfile 2>&1 
   else
      echo "Could not establish internet connection to DNS. Something may be wrong here."   >>./tmp/logfile 2>&1
      nb_problem=$((nb_problem + 1)) 
   fi
}



################################################################################
#
################################################################################
function httpreq
{
   echo "--------------------------------------------------------------------------------"  >>./tmp/logfile 2>&1
   echo "Checking for HTTP Connectivity"                                                    >>./tmp/logfile 2>&1

   number=$(curl -s --max-time 2 -I $checkdomain |wc -l)                                    >>./tmp/logfile 2>&1    
   if [ "$number" -gt 0 ]; then
      echo "HTTP connectivity is up"                                                        >>./tmp/logfile 2>&1
   else
      echo "Something is wrong with HTTP connections. Go check it."                         >>./tmp/logfile 2>&1
      nb_problem=$((nb_problem + 1))
   fi
}



################################################################################
################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

function check_internet_connexion 
{


   GW=`/sbin/ip route | awk '/default/ { print $3 }'`
   checkdns=`cat /etc/resolv.conf | awk '/nameserver/ {print $2}' | awk 'NR == 1 {print; exit}'`
   checkdomain=google.com
   nb_problem=0

   {
      echo_for_whiptail "0" "ping gateway"
      pinggateway


      echo_for_whiptail "25" "ping dns"
      pingdns
      sleep 1


      #pingnet


      echo_for_whiptail "50" "port scan"
      portscan
      sleep 1



  #    echo_for_whiptail "75" "http req"
  #    httpreq
  #    sleep 1


      echo_for_whiptail "100" "http req"
      sleep 2

      # set return code
      if [ "$nb_problem" -eq 0 ]; then   ret=0;     else     ret=1;     fi 


   }  |   whiptail --title "check network" --gauge "network " 6 60 0


return $ret

}
