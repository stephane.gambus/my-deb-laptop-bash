


#################################
# in root_setup_dnscrypt_proxy.sh, set better dns providers (maybe externalyse the param in ./lists/......
#################################
now:  server_names = ['cloudflare','cloudflare-ipv6','opendns'] in /etc/dnscrypt-proxy/dnscrypt-proxy.toml

source: https://dnsprivacy.org/wiki/
source: https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Test+Servers




#################################
# install **libu2f-udev** package from backports
#################################
if **libu2f-udev** is present on buster backports, add it in lists/apt_debian_buster-backports_main.list
+ modify root_setup_udev.sh to remove creation of the file /etc/udev/rules.d/70-solo_key.rules
+ modify README.md to explain : 
we install libu2f-udev from backports to have a newer file /usr/lib/udev/rules.d/70-u2f.rules to support more security key devices


#################################
# install firmware-linux (firmwares-linux-nonfree and firmware-misc-nonfree) from backports to match ne newer kernel
#################################œ

exemple: what i do while this package is not availlable in buster-backports 
git clone https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git
sudo cp ./linux-firmware/i915/icl_dmc_ver1_07.bin /lib/firmware/i915/
sudo cp ./linux-firmware/i915/bxt_huc_ver01_8_2893.bin /lib/firmware/i915/
