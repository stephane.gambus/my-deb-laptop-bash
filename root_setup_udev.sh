#!/bin/bash
# root_setup_udev.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################




controls_with_sudo

# solokey (https://docs.solokeys.io/solo/udev/)
# /etc/udev/rules.d/70-solo_key.rules
echo '# Solo'                                                                                                                >  /etc/udev/rules.d/70-solo_key.rules
echo 'SUBSYSTEM=="hidraw", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="a2ca", TAG+="uaccess", MODE="0660", GROUP="plugdev"'  >> /etc/udev/rules.d/70-solo_key.rules


sudo udevadm control --reload-rules && sudo udevadm trigger



exit
