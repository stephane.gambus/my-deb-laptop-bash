#!/bin/bash

# Run in debug mode
#set -x

#trap  stop_on_error ERR

set -e

# source libraries
source libs/all;



################################################################################
#
################################################################################
enable_sound_for_torbrowser_in_apparmor () {

local FILEPATH="/etc/apparmor.d/torbrowser.Browser.firefox"

if [ -e "$FILEPATH" ]; then

   sed -i 's|  deny /etc/machine-id r,|  /etc/machine-id r,|' $FILEPATH
   sed -i 's|  deny /var/lib/dbus/machine-id r,|  /var/lib/dbus/machine-id r,|' $FILEPATH

   systemctl reload apparmor

else
   echo "the file $FILEPATH is not found, so we do nothing..."

fi


}



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo



# enable sound for torbrowser in apparmor
enable_sound_for_torbrowser_in_apparmor



exit
