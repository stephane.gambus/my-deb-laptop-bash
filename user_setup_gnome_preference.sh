#!/bin/bash
# user_setup_gnome_preference.sh 

# Run in debug mode
#set -x

trap stop_on_error ERR


# source libraries
source libs/all;



################################################################################
# set gnome settings from ./lists/gnome_settings.list 
################################################################################
setup_gnome_settings () {

   val_col_1=""
   val_col_2=""
   val_col_3=""


   while read ligne ; do

      val_col_1=$(echo $ligne | awk '{print $1}')
      val_col_2=$(echo $ligne | awk '{print $2}')
      val_col_3=$(echo $ligne | awk '{print $3}')
      val_col_4=$(echo $ligne | awk '{print $4}')

      if [ ! -z "$val_col_4" ]; then
         /usr/bin/gsettings set "$val_col_1" "$val_col_2" "$val_col_3 $val_col_4"
      else
         /usr/bin/gsettings set "$val_col_1" "$val_col_2" "$val_col_3"
      fi

   done    <  ./lists/gnome_settings.list


}


################################################################################
# setup gnome locale
################################################################################
setup_gnome_locale () {
locale=$(cat /etc/default/locale | grep "^LANG=" | cut -d= -f2 | sed 's/"//g')


# set locale
/usr/bin/gsettings set org.gnome.system.locale region "'$locale'"


}



################################################################################
# setup gnome-terminal
################################################################################
setup_gnome_terminal () {


#/usr/bin/gnome-terminal
#proc=$(ps aux | grep -v grep | grep "/usr/lib/gnome-terminal/gnome-terminal-server" | awk  '{ print $2 }')
#sleep 2
#kill -9 $proc


# echo the terminal profilesList
gsettings get org.gnome.Terminal.ProfilesList list


# echo the defaul terminal profile
gsettings get org.gnome.Terminal.ProfilesList default


# put the default terminal profile in a variable
UUID=$(gsettings get org.gnome.Terminal.ProfilesList default | tr -d \')

# set use-theme-colors to false for the default terminal profile
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${UUID}/ use-theme-colors      'false'



}



################################################################################
# setup_gnome_shortcuts 
################################################################################
setup_gnome_shortcuts () {


if  [ "$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings | grep "custom0" | wc -l)" == "0" ]; then

   # Set shortcut Ctrl+Alt+T to launch gnome-terminal	
   gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "[ '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/' ]"

   gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name 'Terminal'
   gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command 'gnome-terminal'
   gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding '<Ctrl><Alt>T'

fi


}



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo


setup_gnome_settings

setup_gnome_locale

setup_gnome_terminal

setup_gnome_shortcuts



exit 
