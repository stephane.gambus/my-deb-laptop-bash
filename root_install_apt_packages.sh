#!/bin/bash
# root_install_apt_packages.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
# check parameters
################################################################################
check_parameters () {

   local list_file="$1"

   # Check $listfile existance. if not, we exit
   if ! [ -e "$list_file" ]; then 
      echo "root_apt_package_install.sh:   first parameter need to be an existing file"
      exit 1
   fi


}



################################################################################
# install packages
################################################################################
install_packages () {


   local filepath="$1"
   local text="$2"
   
   
   # check parameters
   check_parameters "$filepath" "$text"
   
   
   # get nomber of lines of $filepath
   nbline=$(wc -l $filepath | awk '{ print $1 }')
   


   if [ "$nbline" -gt 0  ]; then

      # calculate on percent value
      onepercent=$(echo 100 / $nbline | bc -l)
   
   
      {
   
          i=0
          while read -r line; do
   
              i=$(( i + 1 ))
   
              echo_for_whiptail "$(echo "$onepercent * $i" |bc | cut -d . -f 1)" "$text: $line"
   
              # install the package
              DEBIAN_FRONTEND=noninteractive /usr/bin/apt -q --yes install $line          2>/dev/null
   
          done < <(cat $filepath | awk '{ print $1 }')
   
   
   
          echo_for_whiptail "100" "Installed"
          sleep 1
   
   
      }  | whiptail --title "apt (download and install)" --gauge "Installing" 6 60 0
   

   else 

      echo "The file $filepath is empty, so we do nothing..."   

   fi

}



################################################################################
################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo

#------ buster ------------------


# apt package install (main)
install_packages ./lists/apt_debian_buster_main.list      "main"

# apt package install (contrib)
install_packages ./lists/apt_debian_buster_contrib.list   "contrib"

# apt package install (non-free)
install_packages ./lists/apt_debian_buster_non-free.list  "non-free"


#------ buster-backports --------

# apt package install (backports main)
install_packages ./lists/apt_debian_buster-backports_main.list     "backports main"

# apt package install (backports contrib)
install_packages ./lists/apt_debian_buster-backports_contrib.list  "backports contrib"

# apt package install (backports non-free)
install_packages ./lists/apt_debian_buster-backports_non-free.list "backports non-free"



exit
