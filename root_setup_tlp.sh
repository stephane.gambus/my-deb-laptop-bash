#!/bin/bash
# root_setup_tlp.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;




################################################################################
# configure_etc_default_tlp
################################################################################
configure_etc_default_tlp () {


# before: CPU_HWP_ON_AC=balance_performance
# after:  CPU_HWP_ON_AC=performance
sed -i 's/^CPU_HWP_ON_AC=.*/CPU_HWP_ON_AC=performance/g' /etc/default/tlp


# before: SATA_LINKPWR_ON_AC="med_power_with_dipm max_performance"
# after:  SATA_LINKPWR_ON_AC="max_performance"
sed -i 's/^SATA_LINKPWR_ON_AC=.*/SATA_LINKPWR_ON_AC=max_performance/g' /etc/default/tlp


# before: MAX_LOST_WORK_SECS_ON_BAT=60
# after:  MAX_LOST_WORK_SECS_ON_BAT=15
sed -i 's/^MAX_LOST_WORK_SECS_ON_BAT=.*/MAX_LOST_WORK_SECS_ON_BAT=15/g' /etc/default/tlp



}



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo



packagename="tlp"

# check if the package is installed
state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $packagename || true)

if [ "$state" == "installed" ]; then

   # configure /etc/defaults/tlp
   configure_etc_default_tlp

   # restart tlp service
   /usr/bin/systemctl restart tlp.service


else
   echo "the package $packagename is not installed, so we do nothing"

fi



exit
