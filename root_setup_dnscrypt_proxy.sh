#!/bin/bash
# root_setup_dnscrypt_proxy_for_all_connexions.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
# setup dnscrypt-proxy for all connexions
################################################################################
setup_dnscrypt_proxy_for_all_connexions () {


   path_resolv_conf_file='/etc/resolv.conf'
   path_no_dns_conf_file='/etc/NetworkManager/conf.d/no-dns.conf'

   # remove the file if already present
   if [ -e "$path_no_dns_conf_file" ]; then rm -f "$path_no_dns_conf_file" ; fi

   # we tell to NetworkManager to not use the dns gived by the dhcp
   echo "[main]" | tee -a "$path_no_dns_conf_file"                || echo "error on adding dns=non in "$path_no_dns_conf_file 
   echo "dns=none" | tee -a "$path_no_dns_conf_file"              || echo "error on adding dns=non in "$path_no_dns_conf_file 

   # restart NetworkManager.service to be able to remove resolv.conf
   systemctl restart NetworkManager.service

   # remove the resolv.conf file 
   if [ -e "$path_resolv_conf_file" ]; then rm -f "$path_resolv_conf_file" ; fi

   # we force the dns to be 127.0.2.1 (for dnscrypt-proxy)
   echo "nameserver 127.0.2.1" | tee -a "$path_resolv_conf_file"          || echo "error on 127.0.2.1 in "$path_resolv_conf_file

}


################################################################################
# setup server names in /etc/dnscrypt-proxy/dnscrypt-proxy.toml
################################################################################
setup_server_names () {

   # before:     server_names = ['cloudflare']
   # after:      server_names = ['cloudflare','cloudflare-ipv6','opendns']

   sed -i "s/^server_names =.*/server_names = ['cloudflare','cloudflare-ipv6','opendns']/" /etc/dnscrypt-proxy/dnscrypt-proxy.toml

}


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo


packagename="dnscrypt-proxy"

# check if the package is installed
state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $packagename || true)

if [ "$state" == "installed" ]; then

   # setup /etc/dnscrypt-proxy/dnscrypt-proxy.toml
   setup_server_names

   # setup dnscrypt-proxy for all connexions
   setup_dnscrypt_proxy_for_all_connexions
else
   echo "the package $packagename is not installed, so we do nothing"

fi



exit
