#!/bin/bash
################################################################################
# check.sh : check files
################################################################################


# Run in debug mode
#set -x

trap  stop_on_error ERR


# source libraries
source libs/all




################################################################################
#
################################################################################
ufw_status_check () {


local state_check=0   # state of the check
local target=""
local line=""

target='Default: deny (incoming), allow (outgoing), disabled (routed)'
line="$(ufw status verbose |grep 'Default: deny (incoming), allow (outgoing), disabled (routed)')"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check root_setup_ufw.sh: check 1" "$state_check"



target='224.0.0.0/4/udp            ALLOW IN    Anywhere'
line="$(ufw status verbose |grep '224.0.0.0/4/udp            ALLOW IN    Anywhere')"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check root_setup_ufw.sh: check 2" "$state_check"




target='Anywhere                   ALLOW IN    224.0.0.0/4/udp'
line="$(ufw status verbose |grep 'Anywhere                   ALLOW IN    224.0.0.0/4/udp')"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check root_setup_ufw.sh: check 3" "$state_check"



target='9001/tcp                   ALLOW IN    Anywhere'
line="$(ufw status verbose |grep '9001/tcp                   ALLOW IN    Anywhere')"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check root_setup_ufw.sh: check 4" "$state_check"




target='9001/tcp (v6)              ALLOW IN    Anywhere (v6)'
line="$(ufw status verbose |grep '9001/tcp (v6)              ALLOW IN    Anywhere (v6)')"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check root_setup_ufw.sh: check 5" "$state_check"




}



################################################################################
# check /etc/apt/sources.list
################################################################################
apt_sources_check () {

local state_check=0   # state of the check
local line1="deb http://deb.debian.org/debian/ buster main contrib non-free"
local line2="deb-src http://deb.debian.org/debian/ buster main contrib non-free"
local line3="deb http://security.debian.org/debian-security buster/updates main contrib non-free"
local line4="deb-src http://security.debian.org/debian-security buster/updates main contrib non-free"
local line5="deb http://deb.debian.org/debian buster-backports main contrib non-free"
local line6="deb-src http://deb.debian.org/debian buster-backports main contrib non-free"


state_check=0   
local nb_contrib="$(cat /etc/apt/sources.list |grep "^deb"|grep " contrib" |wc -l)"
local nb_nonfree="$(cat /etc/apt/sources.list |grep "^deb"|grep " non-free" |wc -l)"
local nb_backports="$(cat /etc/apt/sources.list |grep "^deb"|grep " buster-backports" |wc -l)"


if [ "$nb_contrib" -gt "0" ] && [ "$nb_nonfree" -gt "0" ] && [ "$nb_backports" -gt "0" ]; then state_check=1; fi
echo_result "Check /etc/apt/sources.list" "$state_check"


}


################################################################################
# check /etc/apt/sources.list
################################################################################
apt_seccomp_check () { 

state_check=0   
string="$(cat /etc/apt/apt.conf.d/999seccomp | grep 'apt::sandbox::seccomp "true";' | wc -l)"
if [ "$(echo $string)" == "1" ]; then state_check=1; fi
echo_result "Check /etc/apt/apt.conf.d/999seccomp" "$state_check"


}



################################################################################
# check /etc/apt/sources.list
################################################################################
apt_sources_check_old () {

local state_check=0   # state of the check
local line1="deb http://deb.debian.org/debian/ buster main contrib non-free"
local line2="deb-src http://deb.debian.org/debian/ buster main contrib non-free"
local line3="deb http://security.debian.org/debian-security buster/updates main contrib non-free"
local line4="deb-src http://security.debian.org/debian-security buster/updates main contrib non-free"
local line5="deb http://deb.debian.org/debian buster-backports main contrib non-free"
local line6="deb-src http://deb.debian.org/debian buster-backports main contrib non-free"

if [ "$(cat /etc/apt/sources.list |grep "^deb"|head -1)" == "$line1" ]; then
   if [ "$(cat /etc/apt/sources.list |grep "^deb"|head -2 |tail +2)" == "$line2" ]; then

      if [ "$(cat /etc/apt/sources.list |grep "^deb"|head -3 |tail +3)" == "$line3" ]; then

         if [ "$(cat /etc/apt/sources.list |grep "^deb"|head -4 |tail +4)" == "$line4" ]; then

            if [ "$(cat /etc/apt/sources.list |grep "^deb"|head -4 |tail +5)" == "$line5" ]; then

               if [ "$(cat /etc/apt/sources.list |grep "^deb"|head -4 |tail +6)" == "$line6" ]; then

                  state_check=1

	       fi

            fi 

         fi

      fi

   fi
fi


echo_result "Check /etc/apt/sources.list" "$state_check"



}



################################################################################
# check apt package installed
################################################################################
apt_package_installed_check () {

local filetocheck=$1
local text=$2

local state_check=0   # state of the check
local ligne=""
local state=""
local cpt_ko=0

awk '{print $1}' $filetocheck | while read ligne ; do

   state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $ligne || true)
   if [ "$state" != "installed" ]; then
      cpt_ko=$(( "$cpt_ko" + 1 ))
      echo_result "Check apt package $text "$ligne "$state_check"
   fi

done


if [ "$cpt_ko" -eq 0 ]; then
   state_check=1
   echo_result "Check apt package $text" "$state_check"
fi


}



################################################################################
# flatpack remote check
################################################################################
flatpack_remote_check () {

local state_check=0   # state of the check

if [ "$(flatpak remotes |grep "^flathub" |wc -l)" -gt 0 ]; then state_check=1 ;  fi 
echo_result "Check flatpak remote" "$state_check"

}



################################################################################
# grub /etc/default/grub 
################################################################################
grub_check () {

local state_check=0   # state of the check
local target=""
local line=""

state_check=0 
target="1"
line=$(cat /etc/default/grub | grep '^GRUB_TIMEOUT=0' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result "Check /etc/default/grub : GRUB_TIMEOUT=0" "$state_check"


state_check=0 
target="quiet "
line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |grep $target | sed 's/\"//g' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result "Check /etc/default/grub : quiet" "$state_check"


state_check=0 
target=" splash"
line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |grep $target | sed 's/\"//g' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result "Check /etc/default/grub : splash" "$state_check"



}



################################################################################
# bashrc check
################################################################################
bashrc_uncomment_check () {


local state_check=0   # state of the check
local path_file='/root/.bashrc'


# if the file exist
if [ -e "/root/.bashrc" ]; then

   if [ "$(grep "^export LS_OPTIONS=" "$path_file" | wc -l)" -eq 1 ];then

   	   if [ "$(grep "^alias" "$path_file" | wc -l)" -eq 6 ]; then
              state_check=1 
           fi

   fi

fi

echo_result "Check uncomment "$path_file "$state_check"


}


################################################################################
# bashrc check
################################################################################
bashrc_completion_check () {

local state_check=0   # state of the check
local path_file='/root/.bashrc'
local cpt=0


# if the file exist
if [ -e "/root/.bashrc" ]; then
      if [ "$(cat /root/.bashrc | grep 'if ! shopt -oq posix; then' | wc -l)" == 1 ]; then   state_check=1; fi
fi


echo_result "Check completion "$path_file "$state_check"


}




################################################################################
# vimrc root check  
################################################################################
vimrc_root_check () {


local state_check=0   # state of the check

local line1="source /usr/share/vim/vim81/defaults.vim"
local line2="set syntax=on"
local line3="set mouse=r"

local path_file='/root/.vimrc'

if [ -e "$path_file" ]; then

   if [ "$(cat $path_file |head -1)" == "$line1" ]; then

      if [ "$(cat $path_file |head -2 |tail +2)" == "$line2" ]; then

         if [ "$(cat $path_file |head -3 |tail +3)" == "$line3" ]; then
            state_check=1
         fi

      fi

   fi

fi

echo_result "Check "$path_file "$state_check"


}



################################################################################
# tlp check 
################################################################################
tlp_check () {

local state_check=0   # state of the check
local cpt_ok=0 
local target=""
local line=""


target="CPU_HWP_ON_AC=performance"
line=$(cat /etc/default/tlp |grep "^CPU_HWP_ON_AC=")
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


target="SATA_LINKPWR_ON_AC=max_performance"
line=$(cat /etc/default/tlp |grep "^SATA_LINKPWR_ON_AC=")
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi

target="MAX_LOST_WORK_SECS_ON_BAT=15"
line=$(cat /etc/default/tlp |grep "^MAX_LOST_WORK_SECS_ON_BAT=")
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi



if [ "$cpt_ok" -eq 3 ]; then   state_check=1 ;  fi
echo_result "Check /etc/default/tlp" "$state_check"


}


################################################################################
# network manager check
################################################################################
network_manager_check () {


local state_check=0   # state of the check
local cpt_ok=0
local target=""
local line=""


target="[connection]"
line=$(cat /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf | head -1)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


target="wifi.powersave = 3"
line=$(cat /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf | tail +2)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


if [ "$cpt_ok" -eq 2 ]; then   state_check=1 ;  fi
echo_result "Check /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf" "$state_check"


}



################################################################################
# plymouth check 
################################################################################
plymouth_check () {


local state_check=0   # state of the check
local target="Theme=futureprototype"
local line=$(cat /etc/plymouth/plymouthd.conf | grep '^Theme=' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
if [ "$line" == "$target" ]; then state_check=1 ; fi


echo_result "Check /etc/plymouth/plymouthd.conf" "$state_check"



}



################################################################################
# i915 check
################################################################################
i915_check () {


local state_check=0   # state of the check
local cpt_ok=0
local target=""
local line=""



target="options i915 modeset=1"
line=$(cat /etc/modprobe.d/i915.conf | grep '^options i915 modeset=1' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


target="options i915 fastboot=1"
line=$(cat /etc/modprobe.d/i915.conf | grep 'options i915 fastboot=1' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


target="options i915 enable_guc=3"
line=$(cat /etc/modprobe.d/i915.conf | grep '^options i915 enable_guc=3' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi



#target="options i915 enable_psr=1"
#line=$(cat /etc/modprobe.d/i915.conf | grep 'options i915 enable_psr=1' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
#if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


#target="options i915 enable_fbc=1"
#line=$(cat /etc/modprobe.d/i915.conf | grep 'options i915 enable_fbc=1' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
#if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


#target="options i915 enable_gvt=1"
#line=$(cat /etc/modprobe.d/i915.conf | grep 'options i915 enable_gvt=1' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
#if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi




if [ "$cpt_ok" -eq 3 ]; then   state_check=1 ;  fi
echo_result "Check /etc/modprobe.d/i915.conf" "$state_check"



}



################################################################################
# sysctl conf check
################################################################################
sysctl_conf_check () {


local state_check=0   # state of the check
local cpt_ok=0
local target=""
local line=""

target="vm.swappiness=10"
line=$(cat /etc/sysctl.d/99-sysctl.conf | grep 'vm.swappiness=10' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


target="vm.vfs_cache_pressure=50"
line=$(cat /etc/sysctl.d/99-sysctl.conf | grep 'vm.vfs_cache_pressure=50' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


if [ "$cpt_ok" -eq 2 ]; then   state_check=1 ;  fi
echo_result "Check /etc/sysctl.d/99-sysctl.conf" "$state_check"



}



################################################################################
# io scheduler check
################################################################################
io_scheduler_check () {


local state_check=0   # state of the check
local target=""
local line=""

target="block/nvme0n1/queue/scheduler = mq-deadline"
line=$(cat /etc/sysfs.conf | grep 'block/nvme0n1/queue/scheduler = mq-deadline' | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
if [ "$line" == "$target" ]; then state_check=1 ; fi


echo_result "Check /etc/sysfs.conf" "$state_check"


}



################################################################################
# Check /etc/exim4/update-exim4.conf.conf
################################################################################
exim4_check () {

local state_check=0   # state of the check
local target=""
local line=""

target="1"
line=$(cat /etc/exim4/update-exim4.conf.conf | grep "dc_minimaldns='true'" | wc -l)
if [ "$line" == "$target" ]; then state_check=1 ; fi


echo_result "Check /etc/exim4/update-exim4.conf.conf dc_minimaldns='true'" "$state_check"


}



################################################################################
# Check /etc/resolv.conf 
################################################################################
resolv_conf_check () {


local state_check=0   # state of the check
local path_resolv_conf_file='/etc/resolv.conf'


if [ -e "$path_resolv_conf_file" ]; then
   if [ "$(cat "$path_resolv_conf_file" | grep "^nameserver 127.0.2.1" | wc -l)" -eq 1 ]; then 

      if [ "$(cat "$path_resolv_conf_file" | grep "^nameserver " | wc -l)" -eq 1 ]; then 
           state_check=1 
      fi

   fi
fi


echo_result "Check "$path_resolv_conf_file "$state_check"


}



################################################################################
# dnscrypt_proxy_servers_name_check
################################################################################
dnscrypt_proxy_servers_name_check () {

local target
local line
target="server_names = ['cloudflare','cloudflare-ipv6','opendns']"

line=$(grep "^server_names = "  /etc/dnscrypt-proxy/dnscrypt-proxy.toml)
if [ "$line" == "$target" ]; then state_check=1  ; fi

#echo $state_check
#echo "-->"$target"<--"
#echo "-->"$line"<--"

echo_result "Check /etc/dnscrypt-proxy/dnscrypt-proxy.toml" "$state_check"


}



################################################################################
# dnscrypt_proxy_for_all_connexions_check
################################################################################
dnscrypt_proxy_for_all_connexions_check () {


local state_check=0   # state of the check
local path_no_dns_conf_file='/etc/NetworkManager/conf.d/no-dns.conf'


if [ -e "$path_no_dns_conf_file" ]; then
   if [ "$(cat "$path_no_dns_conf_file" | grep "\[main\]" | wc -l)" -eq 1 ]; then 
	if [ "$(cat "$path_no_dns_conf_file" | grep "dns=none" | wc -l)" -eq 1 ]; then 
           state_check=1 
        fi
   fi
fi


echo_result "Check "$path_no_dns_conf_file "$state_check"


}



################################################################################
# group check
################################################################################
usbguard_check () {


local user_name=$1
local state_check=0   # state of the check
local cpt_ok=0


target="1"     
line=$(cat /etc/usbguard/usbguard-daemon.conf | grep '^IPCAllowedUsers=' | grep $user_name | wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 ))  ; fi


target="1"     
line=$(cat /etc/usbguard/usbguard-daemon.conf | grep '^IPCAllowedGroups=' | grep $user_name | wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 ))  ; fi



if [ "$cpt_ok" == "2" ]; then state_check=1  ; fi

echo_result "Check /etc/usbguard/usbguard-daemon.conf" "$state_check"



}




################################################################################
# apparmor_check 
################################################################################
apparmor_check () {


local state_check=0   # state of the check
local cpt_ok=0


# --- check status ---
target="1"     
line=$(aa-status | grep '^0 profiles are in complain mode.' | wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 ))  ; fi


target="1"     
line=$(aa-status | grep '^0 processes are in complain mode.' | wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 ))  ; fi


target="1"     
line=$(aa-status | grep '^0 processes are unconfined but have a profile defined.' | wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 ))  ; fi



if [ "$cpt_ok" == "3" ]; then state_check=1  ; fi

echo_result "Check Apparmor aa-status" "$state_check"




# --- check /etc/apparmor/parser.conf ---
state_check=0
target="1"     
line=$(cat /etc/apparmor/parser.conf | grep "^write-cache" | wc -l)
if [ "$line" == "$target" ]; then state_check=1 ; fi

echo_result "Check Apparmor /etc/apparmor/parser.conf write-cache" "$state_check"


}


################################################################################
# unattended_upgrades_status_check
################################################################################
unattended_upgrades_status_check () {

local state_check=0   # state of the check
local cpt_ok=0 
local target=""
local line=""


target="1"
line=$(grep "^        \"origin=Debian,codename=\${distro_codename}-updates\";"             /etc/apt/apt.conf.d/50unattended-upgrades  |wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


target="1"
line=$(grep "^        \"origin=Debian,codename=\${distro_codename}-proposed-updates\";"    /etc/apt/apt.conf.d/50unattended-upgrades  |wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


target="1"
line=$(grep "^Unattended-Upgrade::Remove-Unused-Dependencies \"false\";"                   /etc/apt/apt.conf.d/50unattended-upgrades  |wc -l)
if [ "$line" == "$target" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi



if [ "$cpt_ok" == 3 ]; then state_check=1; fi

echo_result "Check unattended-upgrades: /etc/apt/apt.conf.d/50unattended-upgrades" "$state_check"


}



################################################################################
# torbrowser-launcher_check
################################################################################
torbrowser-launcher_check() {


local state_check=0   # state of the check
local package="torbrowser-launcher"
local state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $package || true)

if [ "$state" == "installed" ]; then state_check=1  ; fi


echo_result "Check root_setup_torbrowser-launcher.sh" "$state_check"

}





################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

user=$(/usr/bin/getent group sudo | cut -d: -f4)


controls_with_sudo 


# checks for root
apt_sources_check 
apt_seccomp_check 

apt_package_installed_check ./lists/apt_debian_buster_main.list               "main"
apt_package_installed_check ./lists/apt_debian_buster_contrib.list            "contrib"
apt_package_installed_check ./lists/apt_debian_buster_non-free.list           "non-free"
apt_package_installed_check ./lists/apt_debian_buster-backports_main.list     "backports main"
apt_package_installed_check ./lists/apt_debian_buster-backports_contrib.list  "backports contrib"
apt_package_installed_check ./lists/apt_debian_buster-backports_non-free.list "backports non-free"

flatpack_remote_check
grub_check 
bashrc_uncomment_check
bashrc_completion_check
vimrc_root_check
ufw_status_check
tlp_check
network_manager_check
plymouth_check
i915_check
sysctl_conf_check
io_scheduler_check
exim4_check
resolv_conf_check 
dnscrypt_proxy_servers_name_check
dnscrypt_proxy_for_all_connexions_check
usbguard_check $user
apparmor_check 
unattended_upgrades_status_check
torbrowser-launcher_check



exit 0
