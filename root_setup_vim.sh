#!/bin/bash
# all_setup_vim.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo


if [ -e "$HOME/.vimrc" ]; then
   echo "the file $HOME/.vimrc is already present"
else
   echo "source /usr/share/vim/vim81/defaults.vim"   | tee -a   $HOME/.vimrc
   echo "set syntax=on"                              | tee -a   $HOME/.vimrc
   echo "set mouse=r"                                | tee -a   $HOME/.vimrc
fi


if [ -e "/etc/skel/.vimrc" ]; then
   echo "the file /etc/skel/.vimrc is already present"
else
   echo "source /usr/share/vim/vim81/defaults.vim"   | tee -a   /etc/skel/.vimrc
   echo "set syntax=on"                              | tee -a   /etc/skel/.vimrc
   echo "set mouse=r"                                | tee -a   /etc/skel/.vimrc
fi


exit
