&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# my-deb-laptop 

This project customizes a laptop or a desktop from a fresh standard install of Debian GNU/linux 10 (Buster)

&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;

&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# What is done (automated)

&nbsp;
&nbsp;


## Setup apt
- modify /etc/apt/sources.list 
  - comment "deb cdrom:" line
  - add contrib   repository
  - add non-free  repository
  - add backports repository
- create /etc/apt/apt.conf.d/999seccomp
  - add line 'apt::sandbox::seccomp "true";' to enable seccomp sandbox
- apt update
- apt upgrade

&nbsp;
&nbsp;
    
## Install apt packages from debian repositories

| **Repository** | **Package to install** |
|:--------------------------------|:--------------------------------------------------------------------|
| **buster main**                 | [./my-deb-laptop/lists/apt_debian_buster_main.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/apt_debian_buster_main.list) |
| **buster contrib**              | [./my-deb-laptop/lists/apt_debian_buster_contrib.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/apt_debian_buster_contrib.list) |
| **buster non-free**             | [./my-deb-laptop/lists/apt_debian_buster_non-free.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/apt_debian_buster_non-free.list) |
| **buster-backports main**       | [./my-deb-laptop/lists/apt_debian_buster-backports_main.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/apt_debian_buster-backports_main.list)
| **buster-backports contrib**    | [./my-deb-laptop/lists/apt_debian_buster-backports_contrib.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/apt_debian_buster-backports_contrib.list) |
| **buster-backports non-free**   | [./my-deb-laptop/lists/apt_debian_buster-backports_non-free.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/apt_debian_buster-backports_non-free.list) |

&nbsp;
&nbsp;

## Setup flatpak 
- add remote repository **flathub** with the command
```console
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

&nbsp;
&nbsp;

## Install flatpak packages from flathub

| **Repository** | **Package to install** |
|:--------------------------------|:--------------------------------------------------------------------|
| **flathub**                     | [./my-deb-laptop/lists/flatpack_flathub.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/flatpack_flathub.list) |

&nbsp;
&nbsp;

## Setup vim 
- create $HOME/.vimrc for root and user and in /etc/skel/.vimrc
   - add line "source /usr/share/vim/vim81/defaults.vim"
   - add line "set syntax=on"
   - add line "set mouse=r"

&nbsp;
&nbsp;

## Setup bash 
- modify $HOME/.bashrc
   - uncomment aliase                                    for root and user
   - uncomment "export LS_OPTIONS="                      for root
   - uncomment "eval \"\`dircolors\`\""                  for root
   - uncomment "force_color_prompt=yes"                  for user
   - change     "grep --color=" from 'auto' to 'always'  for user
   - enable bash_completion                              for root and user

&nbsp;
&nbsp;

## Setup grub
- modify /etc/default/grub
   - if the processor is intel skylake microarchitecture, add " intel_pstate=skylake_hwp" in "GRUB_CMDLINE_LINUX_DEFAULT=" 
   - modify GRUB_TIMEOUT=**5** to **0** 
   - run the command:
   ```console
   update-grub 
   ```

**note:** to access grub menu when timeout=0, hold down the Shift key at boot time just after POST

&nbsp;
&nbsp;

## Setup plymouth
- set default theme [./my-deb-laptop/lists/plymouth-theme](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/plymouth-theme)

&nbsp;
&nbsp;

## Setup unattended-upgrade
- modify /etc/apt/apt.conf.d/50unattended-upgrades
   - uncomment 'origin=Debian,codename=\${distro_codename}-updates' line
   - uncomment 'origin=Debian,codename=\${distro_codename}-proposed-updates' line
   - uncomment 'Unattended-Upgrade::Remove-Unused-Dependencies "false";' line

&nbsp;
&nbsp;

## Setup Intel i915 optimizations
- if the kernel module **i915** is loaded, create /etc/modprobe.d/i915.conf
   - add the line "options i915 modeset=1"
   - add the line "options i915 fastboot=1"
   - add the line "options i915 enable_guc=3"

**note:** for more information concerning enable_guc=3, read https://01.org/linuxgraphics/downloads/firmware

&nbsp;
&nbsp;

## Setup swapiness and vfs cache
- modify /etc/sysctl.d/99-sysctl.conf
   - add line "vm.swappiness=10"
   - add line "vm.vfs_cache_pressure=50"

&nbsp;
&nbsp;

## Setup IO scheduler 
- if a ssd is present and it support mq-deadline io scheduler, we activate it adding a line in /etc/sysfs.conf 

&nbsp;
&nbsp;

## Setup exim4 
- modify /etc/exim4/update-exim4.conf.conf
   - change dc_minimaldns='false' by 'true' to speedup boot time

&nbsp;
&nbsp;

## Setup tlp 
- modify /etc/default/tlp 
   - replace CPU_HWP_ON_AC=**balance_performance** by **performance**
   - replace SATA_LINKPWR_ON_AC=**"med_power_with_dipm max_performance"** by **"max_performance"**
   - replace MAX_LOST_WORK_SECS_ON_BAT=**60** by **15** 

&nbsp;
&nbsp;

## Setup network manager
- create /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf
   - add the line "[connection]"
   - add the line "wifi.powersave = 3"

&nbsp;
&nbsp;

## Setup gnome preference
- apply parameters defined in [./my-deb-laptop/lists/gnome_settings.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/gnome_settings.list) 

- define the correct locale with those commands
```console
locale=$(cat /etc/default/locale | grep "^LANG=" | cut -d= -f2 | sed 's/"//g')
gsettings set org.gnome.system.locale region "'$locale'"
```
- modify the background color of gnome-terminal from white to black with those commands
```console
gsettings get org.gnome.Terminal.ProfilesList list
gsettings get org.gnome.Terminal.ProfilesList default
UUID=$(gsettings get org.gnome.Terminal.ProfilesList default | tr -d \')
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${UUID}/ use-theme-colors      'false'
```
- add shortcut Ctrl+Alt+T to launch gnome-terminal whith those commands
```console
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "[ '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/' ]"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name 'Terminal'
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command 'gnome-terminal'
   gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding '<Ctrl><Alt>T'
```

&nbsp;
&nbsp;


## Setup mimeapps
- copy [./my-deb-laptop/lists/mimeapps.list](https://gitlab.com/stephane.gambus/my-deb-laptop/blob/master/lists/mimeapps.list) in $HOME/.config/mimeapps.list to set :

  - vlc       as default video app
  - rhythmbox as default sound app


&nbsp;
&nbsp;

## Setup udev
- create file /etc/udev/rules.d/70-solo_key.rules to support solokeys (https://solokeys.com/)


&nbsp;
&nbsp;

## Setup usbguard 
- modify /etc/usbguard/usbguard-daemon.conf
   - add the username in IPCAllowedUsers=
   - enable usbguard.service

**note:** for more information about usbgard, read https://usbguard.github.io/

&nbsp;
&nbsp;

## Setup usbguard_applet_qt defaults
- create $HOME/.config/USBGuard/usbguard-applet-qt.conf for user

&nbsp;
&nbsp;

## Setup dnscrypt_proxy
-  modify /etc/dnscrypt-proxy/dnscrypt-proxy.toml
   - replace server_names = ['cloudflare']  by  server_names = ['cloudflare','cloudflare-ipv6','opendns']
- setup dnscrypt-proxy for all connexions
  - create /etc/NetworkManager/conf.d/no-dns.conf to tell NetworkManager to do not use the dns gived by the dhcp
     - add the line "[main]"
     - add the line "dns=none"
   - restart NetworkManager.service
   - Modify /etc/resolv.conf to force the dns to use dnscrypt-proxy 
      - replace everithing by "nameserver 127.0.2.1"

&nbsp;
&nbsp;

## Setup apparmor
- enforce the apparmor security with the command 
   ```console
   aa-enforce /etc/apparmod.d/*
   ```
- modify /etc/apparmor/parser.conf to speed-up AppArmor start by caching profiles
   - uncomment write-cache with the command
   ```console
   sed -i 's|^#write-cache|write-cache|' /etc/apparmor/parser.conf
   ```

**note:** source https://wiki.archlinux.org/index.php/AppArmor

&nbsp;
&nbsp;

## Setup ufw
- send ufw commands to:
   - enable ufw
   - default deny incoming
   - default allow outgoing 
   - allow udp multicast packets incomming and outgoing
   - allow tor incomming

&nbsp;
&nbsp;

## Setup initramfs
- modify /etc/initramfs-tools/initramfs.conf
   - change MODULES=most by MODULES=dep to retuce the size of initramfs to speed up boot time
   - update-initramfs -u

&nbsp;
&nbsp;

## Setup firefox
- add custom parameters in the user.js file of the user
   - layout.css.devPixelsPerPx      1.3
   - geo.enabled                    false
   - browser.startup.homepage       https://duckduckgo.com/
   - media.gmp-gmpopenh264.enabled  true                    (needed by netflix)
   - media.eme.enabled              true                    (needed by netflix)

&nbsp;
&nbsp;

## Setup torbrowser-launcher
   - install torbrowser-launcher from buster-backports repository

   - enable sound in apparmor profile /etc/apparmor.d/torbrowser.Browser.firefox
      - change "  deny /etc/machine-id r,"           by   "  /etc/machine-id r,"              (remove deny)
      - change "  deny /var/lib/dbus/machine-id r,"  by   "  /var/lib/dbus/machine-id r,"     (remove deny)

&nbsp;
&nbsp;

## Setup keyboard
- replace XKBOPTIONS="" by "nbsp:none" in /etc/default/keyboard


&nbsp;
&nbsp;
&nbsp;
&nbsp;

&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# Script to run on demand

&nbsp;
&nbsp;

## To increase Security and Privacy

- script **root_setup_apt_secure.sh** to use https instead of http *and* use tor to acces repositories
   - modify /etc/apt/sources.list
      - for all repositories in /etc/apt/sources.list, if it is accessible via https, change 'http' by 'https'
      - for all repositories in /etc/apt/sources.list, if it is accessible via tor, change 'deb http{s}' by 'deb tor+http{s}'
&nbsp;
- script **root_setup_grub_system_secure.sh** to add in grub some kernel boot param
   - add " mds=full,nosmt slub_debug=P page_poison=1 slab_nomerge pti=on" in "GRUB_CMDLINE_LINUX_DEFAULT="

**note:** source https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings#kernel_command_line_options
                 https://www.youtube.com/watch?v=N-pvLMHtRSA
                 https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html

&nbsp;
&nbsp;

- script **root_setup_wifi_macadress_randomize.sh** to use a random wifi macaddress who never change for a wifi spot
   - create /etc/NetworkManager/conf.d/00-macrandomize.conf
      - add line "[device]"
      - add line "wifi.scan-rand-mac-address=yes"
      - add line ""
      - add line "[connection]"
      - add line "wifi.cloned-mac-address=stable"
      - add line "ethernet.cloned-mac-address=stable"
      - add line "connection.stable-id=\${CONNECTION}/\${BOOT}"

**note:** sometime, some wifi spots needs the real macaddress of the device.
   - to set the real macaddress of a device
      - list the wifi network manager configurations with the command 
         ```console
         nmcli c | grep wifi
         ```
      - identify the uuid of the spot you want to define the real macadress
      - show detail of the uuid with the command
        ```console
        nmcli c show <uuid>
        ```
      - set the real mac address with the command   
        ```console
        nmcli c modify <uuid> 802-11-wireless.cloned-mac-address permanent
        ```

&nbsp;
&nbsp;

## To be able to use Docker containers

- script **root_setup_docker.sh** to install and configure docker
   - install docker.io package
   - add the user in the docker group

&nbsp;
&nbsp;

## For a better gnome desktop experience

- script **user_install_gnomeshell_ext_gnomenu.sh**
   - open gnome-software with search on gnome shell extension  **Gno-menu** (need to click on install and quit gnome-software) 
   - enable the extension gnomenu and disable the extension app-menu
   - configure **gno-menu** extension with those commands
```console
schemadir="$HOME/.local/share/gnome-shell/extensions/gnomenu@panacier.gmail.com/schemas/"
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-workspaces       'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-panel-menu-arrow 'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-panel-view       'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-panel-apps       'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-apps-icon   'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-apps-label  'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-view-label  'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-menu-label  'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-view-icon   'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-useroptions      'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-shortcuts        'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   panel-menu-icon-name  "['debian-swirl']"
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-menu-label  'true'
```

&nbsp;
&nbsp;

- script **user_install_gnomeshell_ext_removeaccessibility.sh**
   - open gnome-software with search on gnome shell extension  **Remove Accessibility** (need to click on install and quit gnome-software) 
   - enable the extension

&nbsp;
&nbsp;

- script **user_install_gnomeshell_ext_notoplefthotcorner.sh**
   - open gnome-software with search on gnome shell extension  **No Topleft Hot Corner** (need to click on install and quit gnome-software) 
   - enable the extension

&nbsp;
&nbsp;

## For french user

- script **user_install_firefox_grammalecte_extension.sh** to download and install in firefox the **grammalecte-fr** extension
- script **user_install_libreoffice_grammalecte_plugin.sh** to download and install in libreoffice the **grammalecte-fr** plugin
- script **user_set_dirs_locale_FR_to_US.sh** to renames the user directories from french names to english names
   - rename   "Bureau"          to  "Desktop"
   - rename   "Images"          to  "Pictures"
   - rename   "Modèles"         to  "Templates"
   - rename   "Musique"         to  "Music"
   - rename   "Téléchargements" to  "Downloads"
   - rename   "Vidéos"          to  "Videos"

&nbsp;
&nbsp;
&nbsp;
&nbsp;


&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# Instructions

&nbsp;
&nbsp;

## prerequisite 
- install Debian 10 (Buster) 
  - enable secureboot in bios
  - do no define root password during install
  - choose encryted LVM partition
  - select gnome desktop

- install git package
```console
  sudo apt install git
```

&nbsp;
&nbsp;

## download my-deb-laptop scripts
```console
git clone https://gitlab.com/stephane.gambus/my-deb-laptop.git
```

&nbsp;
&nbsp;

## check files and customize what you want (optional)
- packages to install with apt  (remove the lines for the packages you don't want)
   - ./lists/apt_debian_buster_main.list
   - ./lists/apt_debian_buster_contrib.list
   - ./lists/apt_debian_buster_non-free.list
   - ./lists/apt_debian_buster-backports_main.list
   - ./lists/apt_debian_buster-backports_contrib.list
   - ./lists/apt_debian_buster-backports_non-free.list
- packages to install with flatpak from flathub
   - ./my-deb-laptop/lists/flatpack_flathub.list
- plymouth theme
   - ./my-deb-laptop/lists/plymouth-theme
- gnome settings
   - ./my-deb-laptop/lists/gnome_settings.list
- mime
   - ./my-deb-laptop/lists/xdg_mime.list

- scripts called by run.sh (remove the lines for the scripts you don't want)
   - ./my-deb-laptop/lists/scripts.list

&nbsp;
&nbsp;

## run the automated program
```console
  cd my-deb-laptop
  sudo ./run.sh
```

&nbsp;
&nbsp;

## in run-on-demand,  choose and run the scripts you want (optional)
```console
  cd run-on-demand
  sudo ./<root_*.sh>
  or
  ./<user_*.sh> 
```
