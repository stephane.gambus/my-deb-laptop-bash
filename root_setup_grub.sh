#!/bin/bash
# root_setup_grub.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
# setup GRUB_CMDLINE_LINUX_DEFAULT=
################################################################################
setup_grub_cmdline_linux_default () {


string_linux_default="quiet splash"                                                                                 

# if (the processor manufacturer is Intel) and (family is generation 6 Skylake), add grub boot param: intel_pstate=skylake_hwp"
cpu_familly_number="$(cat /proc/cpuinfo | grep -i "^cpu family" | awk -F": " '{print $2}' | head -1 | sed 's/ \+/ /g')"

if [ "$(dmidecode -s processor-manufacturer)" == "Intel(R) Corporation" ]; then
	if [ "$cpu_familly_number" == "6" ]; then
      string_linux_default=$string_linux_default" intel_pstate=skylake_hwp"
   fi
fi


local SEARCH='^GRUB_CMDLINE_LINUX_DEFAULT=.*'
local REPLACE='GRUB_CMDLINE_LINUX_DEFAULT=\"'$string_linux_default'\"'
local FILEPATH="/etc/default/grub"
sed -i "s;$SEARCH;$REPLACE;" $FILEPATH
echo $REPLACE


}



################################################################################
# setup GRUB_CMDLINE_LINUX_DEFAULT=
################################################################################
setup_grub_timeout () {


local FILEPATH="/etc/default/grub"
sed -i 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/g' $FILEPATH

echo "set GRUB_TIMEOUT=0 in $FILEPATH" 


}


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo


setup_grub_cmdline_linux_default


setup_grub_timeout


# update grub
/usr/sbin/update-grub                                                                      



exit
