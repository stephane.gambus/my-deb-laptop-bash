#!/bin/bash
# user_setup_firefox.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo


path_profile=""        # Path to de firefox profile directory
path_profile_ini=""    # Path to de firefox profile.ini file
filename_default=""    # filename unique for user
path_user_js=""        # Path to de firefox user.js file

pkill -f firefox-esr && true

# open firefox to create a initial profile, sleep 2 seconds, and after kill firefox
/usr/lib/firefox-esr/firefox-esr -headless & >>./tmp/logfile 2>&1 
sleep 2
pkill -f firefox-esr
#kill $!  >>./tmp/logfile 2>&1


path_profile="$HOME/.mozilla/firefox/"
path_profile_ini=$path_profile'profiles.ini'
filename_default=$(grep '^Path=' "$path_profile_ini" | grep .default | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g'  | cut -d= -f2) 
path_user_js=$path_profile''$filename_default'/user.js'

echo "path_profile      = -->"$path_profile"<--"
echo "path_profile_ini  = -->"$path_profile_ini"<--"
echo "filename_default  = -->"$filename_default"<--"
echo "path_user_js      = -->"$path_user_js"<--"
 
# if the user.js file does not exist
if [ ! -e "$path_user_js" ]; then

   # add custom parameters in the user.js file of the user

   # [TODO] fine tune depending on the display size and the resolution
   echo 'user_pref("layout.css.devPixelsPerPx", "1.3");'                    | tee -a  $path_user_js

   echo 'user_pref("geo.enabled", false);'                                  | tee -a  $path_user_js

   echo 'user_pref("browser.startup.homepage", "https://duckduckgo.com/");' | tee -a  $path_user_js

   # to activate the plugin OpenH264 from cisco (used by netflix)
   echo 'user_pref("media.gmp-gmpopenh264.enabled", true);'                 | tee -a  $path_user_js

   # active DRM (used by netflix)
   echo 'user_pref("media.eme.enabled", true);'                             | tee -a  $path_user_js

else
   echo "the file user.js already exist, so we do nothing"
fi




exit
