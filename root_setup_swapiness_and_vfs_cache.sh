#!/bin/bash
# root_setup_swapiness_and_vfs_cache.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo


if [ "$(cat /etc/sysctl.d/99-sysctl.conf |grep 'vm.swappiness=' | wc -l)" -ge "1" ]; then
   echo "The line \"vm.swappiness=\" is already present in /etc/sysctl.d/99-sysctl.conf"                      
else
   echo "vm.swappiness=10" | tee -a /etc/sysctl.d/99-sysctl.conf
fi


if [ "$(cat /etc/sysctl.d/99-sysctl.conf |grep 'vm.vfs_cache_pressure=' | wc -l)" -ge "1" ]; then
   echo "The line \"vm.vfs_cache_pressure=\" is already present in /etc/sysctl.d/99-sysctl.conf"              
else
   echo "vm.vfs_cache_pressure=50" | tee -a /etc/sysctl.d/99-sysctl.conf
fi



/usr/sbin/sysctl -p /etc/sysctl.d/99-sysctl.conf 



exit
