#!/bin/bash
# root_setup_unattended-upgrade.sh


# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all



################################################################################
# configure /etc/apt/apt.conf.d/50unattended-upgrades
################################################################################
configure_50unattended_upgrades () {

path_file='/etc/apt/apt.conf.d/50unattended-upgrades'


sed -i 's|^\/\/      "origin=Debian,codename=${distro_codename}-updates";|        "origin=Debian,codename=${distro_codename}-updates";|'                   $path_file
sed -i 's|^\/\/      "origin=Debian,codename=${distro_codename}-proposed-updates";|        "origin=Debian,codename=${distro_codename}-proposed-updates";|' $path_file

sed -i 's|^//Unattended-Upgrade::Remove-Unused-Dependencies "false";|Unattended-Upgrade::Remove-Unused-Dependencies "false";|'                             $path_file




}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo


packagename="unattended-upgrades"

# check if the package is installed
state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $packagename || true)

if [ "$state" == "installed" ]; then

   # configure /etc/apt/apt.conf.d/50unattended-upgrades
   configure_50unattended_upgrades


   # restart the service
   /usr/bin/systemctl restart unattended-upgrades.service


   # test
   #unattended-upgrades -v


else
   echo "the package $packagename is not installed, so we do nothing"

fi




exit
