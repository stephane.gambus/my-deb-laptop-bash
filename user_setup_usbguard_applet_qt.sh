#!/bin/bash
# user_usbguard_applet_qt_conf.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo


if [ -e "$HOME/.config/USBGuard/usbguard-applet-qt.conf" ]; then
   echo "$HOME/.config/USBGuard/usbguard-applet-qt.conf is  already present, so we do nothing"
else

   mkdir $HOME/.config/USBGuard
   echo "[DeviceDialog]"                   |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "DecisionIsPermanent=false"        |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "DecisionMethod=0"                 |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "DefaultDecision=1"                |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "DefaultDecisionTimeout=60"        |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "MaskSerialNumber=true"            |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "RandomizeWindowPosition=true"     |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "ShowRejectButton=false"           |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo ""                                 |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "[Notifications]"                  |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "Allowed=true"                     |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "Blocked=true"                     |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "IPCStatus=false"                  |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "Inserted=true"                    |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "Present=false"                    |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "Rejected=true"                    |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf
   echo "Removed=false"                    |   tee -a  $HOME/.config/USBGuard/usbguard-applet-qt.conf

fi



exit
