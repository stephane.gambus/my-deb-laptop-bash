#!/bin/bash
# root_setup_usbguard.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
# setup_usbguard_daemon
################################################################################
setup_usbguard_daemon () {
	
local user=$(get_user)

if ! [ -z "$user" ]; then

   sed -i 's/^IPCAllowedUsers=.*/IPCAllowedUsers=root '$user'/g' /etc/usbguard/usbguard-daemon.conf

   sed -i 's/^IPCAllowedGroups=.*/IPCAllowedGroups=root '$user'/g' /etc/usbguard/usbguard-daemon.conf

   systemctl enable usbguard-dbus.service

   systemctl restart usbguard-dbus.service

else

   echo "username parameter is empty, so we do nothing..."

fi


}



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo



packagename="usbguard"

# check if the package is installed
state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $packagename || true)

if [ "$state" == "installed" ]; then

   setup_usbguard_daemon 

else
   echo "the package $packagename is not installed, so we do nothing"

fi




exit
