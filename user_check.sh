#!/bin/bash
################################################################################
# verif.sh : check files
################################################################################


# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all




################################################################################
# Packages to install in flatpak from flathub
################################################################################
flatpak_package_installation_check () {

local state_check=0  

# read the packages file
while read ligne ; do

   state_check=0   

   package_name=$(echo $ligne | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')
   
   if  [ "$(flatpak list | grep "$package_name" | wc -l)" -ne 0 ]; then state_check=1 ; fi   

   echo_result "Check flatpack: "$package_name "$state_check"


done <  ./lists/flatpack_flathub.list


}



################################################################################
# firefox Check
################################################################################
firefox_check () {

local user_name=$1
local state_check=0       # state of the check
local cpt_ok=0
local target=""
local line=""

local path_profile        # Path to de firefox profile directory
local path_profile_ini    # Path to de firefox profile.ini file
local filename_default    # filename unique for user
local path_user_js        # Path to de firefox user.js file

path_profile='/home/'$user_name'/.mozilla/firefox/'
path_profile_ini=$path_profile'profiles.ini'
filename_default=$(grep '^Path=' "$path_profile_ini" | grep .default | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g'  | cut -d= -f2) 
path_user_js=$path_profile''$filename_default'/user.js'


# if the user.js file does not exist
if [ -e "$path_user_js" ]; then

   target="layout.css.devPixelsPerPx"
   line=$(cat "$path_user_js" | grep $target |wc -l)
   if [ "$line" == "1" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


   target="geo.enabled"
   line=$(cat "$path_user_js" | grep $target |wc -l)
   if [ "$line" == "1" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


   target="browser.startup.homepage"
   line=$(cat "$path_user_js" | grep $target |wc -l)
   if [ "$line" == "1" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi


   target="media.gmp-gmpopenh264.enabled"
   line=$(cat "$path_user_js" | grep $target |wc -l)
   if [ "$line" == "1" ]; then cpt_ok=$(( "$cpt_ok" + 1 )) ; fi



fi

if [ "$cpt_ok" -eq 4 ]; then  state_check=1  ;  fi
echo_result "Check"$path_user_js "$state_check"



}


################################################################################
# setup vim editor 
################################################################################
vim_check () {

local user_name=$1
local state_check=0   # state of the check
local line1="source /usr/share/vim/vim81/defaults.vim"
local line2="set syntax=on"
local line3="set mouse=r"

local path_file='/home/'$user_name'/.vimrc'

if [ -e "$path_file" ]; then

   if [ "$(cat $path_file |head -1)" == "$line1" ]; then

      if [ "$(cat $path_file |head -2 |tail +2)" == "$line2" ]; then

         if [ "$(cat $path_file |head -3 |tail +3)" == "$line3" ]; then
            state_check=1
         fi

      fi

   fi

fi

echo_result "Check "$path_file "$state_check"


}



################################################################################
# bash Check
################################################################################
bash_uncomment_check () {

local user_name=$1
local state_check=0   # state of the check
local line1="force_color_prompt=yes"
local line2="alias"
local line3="grep --color=always"

local path_file='/home/'$user_name'/.bashrc'

if [ -e "$path_file" ]; then 

   if [ "$(cat $path_file | grep '^'$line1 |wc -l)" -ge "1" ]; then

      if [ "$(cat $path_file | grep '^'$line2 |wc -l)" -ge "1"  ]; then

         if [ "$(cat $path_file | grep $line3 |wc -l)" -ge "1"  ]; then
            state_check=1
         fi

      fi

   fi


fi 

echo_result "Check "$path_file  "$state_check"

}



################################################################################
# 
################################################################################
gnome_gsettings_check () {

local state_check=0   # state of the check
local val_col_1=""
local val_col_2=""
local val_col_3=""
local val_col_4=""
local actual_gesttings_value=""
local var_to_check="" 
local ext_01=0
local ext_02=0
local ext_03=0
local ext_04=0
local ext_05=0
local ext_06=0
local ext_07=0
local ext_08=0
local ext_09=0
local ext_10=0
local ext_11=0
local ext_12=0


while read ligne ; do

   unset val_col_1
   unset val_col_2
   unset val_col_3
   unset val_col_4


   # read $ligne in file
   val_col_1=$(echo $ligne | awk '{print $1}')
   val_col_2=$(echo $ligne | awk '{print $2}')
   val_col_3=$(echo $ligne | awk '{print $3}')
   val_col_4=$(echo $ligne | awk '{print $4}')

   #echo "$ligne"
   #echo "val_col_1=$val_col_1"
   #echo "val_col_2=$val_col_2"
   #echo "val_col_3=$val_col_3"
   #echo "val_col_4=$val_col_4"


   # read actual gsettings
   actual_gesttings_value=""
   actual_gesttings_value=$(/usr/bin/gsettings get "$val_col_1" "$val_col_2" | sed  "s/', '/','/g")

   var_to_check=""
   if [ ! -z "$val_col_4" ]; then
      var_to_check="$val_col_3 $val_col_4"
   else
      var_to_check="$val_col_3"
   fi



   if [ "$val_col_1" == "org.gnome.shell" ] && [ "$val_col_2" == "enabled-extensions" ]; then
      # base
      ext_01="$(echo $var_to_check |grep 'dash-to-panel@jderose9.github.com' | wc -l)"
      ext_02="$(echo $var_to_check |grep 'redshift@tommie-lie.de' | wc -l)"
      ext_03="$(echo $var_to_check |grep 'remove-dropdown-arrows@mpdeimos.com' | wc -l)"
      ext_04="$(echo $var_to_check |grep 'TopIcons@phocean.net' | wc -l)"
      ext_05="$(echo $var_to_check |grep 'alternate-tab@gnome-shell-extensions.gcampax.github.com' | wc -l)"
      ext_06="$(echo $var_to_check |grep 'openweather-extension@jenslody.de' | wc -l)"
      ext_07="$(echo $var_to_check |grep 'ubuntu-appindicators@ubuntu.com' | wc -l)"
      ext_08="$(echo $var_to_check |grep 'EasyScreenCast@iacopodeenosee.gmail.com' | wc -l)"

      # menu
      ext_09="$(echo $var_to_check |grep 'apps-menu@gnome-shell-extensions.gcampax.github.com' | wc -l)"
      ext_10="$(echo $var_to_check |grep 'gnomenu@panacier.gmail.com' | wc -l)"

      # others installed manually in run-on-demand
      ext_11="$(echo $var_to_check |grep 'nohotcorner@azuri.free.fr' | wc -l)"
      ext_12="$(echo $var_to_check |grep 'removeaccesibility@lomegor' | wc -l)"

      if [ "$ext_01" == "1" ] && \
         [ "$ext_02" == "1" ] && \
         [ "$ext_03" == "1" ] && \
         [ "$ext_04" == "1" ] && \
         [ "$ext_05" == "1" ] && \
         [ "$ext_06" == "1" ] && \
         [ "$ext_07" == "1" ] && \
         [ "$ext_08" == "1" ]; then extensions_base=1; fi

      if [ "$ext_09" == "1" ] || [ "$ext_10" == "1" ]; then extensions_menu=1; fi  

      if [ "$extensions_base" == "1" ] && [ "$extensions_menu" == "1" ]; then var_to_check=$actual_gesttings_value; fi
   fi

   if [ "$var_to_check" != "$actual_gesttings_value" ]; then
      # echo "-->"$var_to_check"<--"
      # echo "-->"$actual_gesttings_value"<--"
      state_check=0   
   else
      state_check=1   
   fi

   echo_result "Check $val_col_1 $val_col_2" $state_check


done   <   ./lists/gnome_settings.list



}



################################################################################
# check gnome setting org.gnome.system.locale region
################################################################################
gnome_locale_check () {

local state_check=0   # state of the check
local target=""
local line=""


target="'$(cat /etc/default/locale | grep "^LANG=" | cut -d= -f2 | sed 's/"//g')'"
line="$(/usr/bin/gsettings get org.gnome.system.locale region)"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check org.gnome.system.locale region"  "$state_check"



}



################################################################################
# configure_gnome_default_application
################################################################################
gnome_default_application_check () {

local state_check=0   # state of the check
local target="$(sha512sum ./lists/mimeapps.list | awk '{ print $1 }')"
local line="$(sha512sum $HOME/.config/mimeapps.list | awk '{ print $1 }')"


if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check mimeapp for $HOME/.config/mimeapps.list " $state_check


}




################################################################################
# usbguard_applet_qt_conf_check
################################################################################
usbguard_applet_qt_conf_check () {


local state_check=0   # state of the check
local target=""
local line=""

# compare the checksum sha512 of the output
target='cf7b487f7012306d08e1845ad9197688c83e7e93b829c6e6ef724a4aa66bbcf5dc9f3c0f4d34ff2206307043d4e631f99fdbfe62a794532f42d79a03c47e601e'
line="$(sha512sum  $HOME/.config/USBGuard/usbguard-applet-qt.conf | awk '{ print $1 }')"

if [ "$line" == "$target" ]; then state_check=1 ; fi


echo_result "Check $HOME/.config/USBGuard/usbguard-applet-qt.conf" "$state_check"


}



################################################################################
# logfile_heck
################################################################################
logfile_check () {

local state_check=0   # state of the check
local target="0"
local line=""

line=$(cat tmp/logfile | grep "^stop_on_error:" | wc -l)


if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result "Check tmp/logfile : stop_on_error=$line" "$state_check"


}


################################################################################
################################################################################
################################################################################
###############################              ################################### 
###############################     main     ################################### 
###############################              ################################### 
################################################################################
################################################################################
################################################################################


user=$(whoami)


controls_without_sudo



flatpak_package_installation_check

firefox_check $user

vim_check $user

bash_uncomment_check $user

gnome_gsettings_check

gnome_locale_check

gnome_default_application_check

usbguard_applet_qt_conf_check

logfile_check


exit 0
