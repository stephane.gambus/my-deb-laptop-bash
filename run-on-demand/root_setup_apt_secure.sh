#!/bin/bash
# root_setup_apt.sh 

# Run in debug mode
#set -x

#trap  stop_on_error ERR
#set -e

# source libraries
source libs/all;




################################################################################
# for all uniq repo, if it support https, set https
################################################################################
set_https_to_repo () {


   while read ligne ; do

      # test https connexion
      https_url="https://$ligne"

      trap - ERR
      curl -s $https_url >/dev/null 2>&1  
      https_connexion=$?
      trap  stop_on_error ERR


      # if https connexion is supported by repository
      if [ "$https_connexion" -eq 0 ]; then
         # before: "deb http://"
         # after:  "deb https://"
         sed -i 's|http:\/\/'$ligne'|https:\/\/'$ligne'|g'        /etc/apt/sources.list
      fi


   done <<<  $(cat /etc/apt/sources.list | grep "^deb" | cut -d"/" -f3 | sort |uniq)


}


################################################################################
# for all uniq repo, if it support tor, set tor
################################################################################
set_tor_to_repo () {


   # install prerequisite packages
   /usr/bin/apt --yes install tor apt-transport-tor       >>./tmp/logfile 2>&1


   trap - ERR

   # test http connexion thru tor
   /usr/bin/torsocks curl -s --max-time 4 http://google.com > /dev/null 2>&1
   http_connexion_thru_tor=$?

   # test https connexion thru tor
   /usr/bin/torsocks curl -s --max-time 4 https://google.com > /dev/null 2>&1
   https_connexion_thru_tor=$?

   trap  stop_on_error ERR


   # if http connexion thru tor are ok
   if [ "$http_connexion_thru_tor" -eq 0 ]; then
      # before: "deb http://" 
      # after:  "deb tor+http://"
      sed -i 's|^deb http://|deb tor+http://|'            /etc/apt/sources.list

      # before: "deb-src http://" 
      # after:  "deb-src tor+http://"
      sed -i 's|^deb-src http://|deb-src tor+http://|'    /etc/apt/sources.list
   fi



   # if https connexion thru tor are ok
   if [ "$https_connexion_thru_tor" -eq 0 ]; then
      # before: "deb https://" 
      # after:  "deb tor+https://"
      sed -i 's|^deb https://|deb tor+https://|'          /etc/apt/sources.list

      # before: "deb-src https://" 
      # after:  "deb-src tor+https://"
      sed -i 's|^deb-src https://|deb-src tor+https://|'  /etc/apt/sources.list
   fi


}


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################




controls_with_sudo


{
   trap  stop_on_error ERR


   echo_for_whiptail "0" "setup /etc/apt/sources.list"

   set_https_to_repo                                                                           >>./tmp/logfile 2>&1
   echo_for_whiptail "33" "setup /etc/apt/sources.list"

   set_tor_to_repo                                                                             >>./tmp/logfile 2>&1
   echo_for_whiptail "66" "setup /etc/apt/sources.list"

   apt -q update                                                                               >>./tmp/logfile 2>&1


   echo_for_whiptail "100" "apt upgrade"
   sleep 1

}    |   whiptail --title "apt" --gauge "Configuration " 6 60 0





exit
