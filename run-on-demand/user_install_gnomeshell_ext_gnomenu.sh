#!/bin/bash
# user_install_some_external_gnome_shell_extentions.sh 
# from https://extensions.gnome.org/extension/

# Run in debug mode
#set -x

#trap  stop_on_error ERR

set -e

# source libraries
source libs/all;


################################################################################
# remove the script
################################################################################
setup_gnomenu_extension () {


# Setup gnomenu extension
schemadir="$HOME/.local/share/gnome-shell/extensions/gnomenu@panacier.gmail.com/schemas/"
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-workspaces       'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-panel-menu-arrow 'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-panel-view       'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-panel-apps       'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-apps-icon   'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-apps-label  'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-view-label  'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-menu-label  'false'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-view-icon   'false'

/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-useroptions      'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   hide-shortcuts        'true'
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   panel-menu-icon-name  "['debian-swirl']"
/usr/bin/gsettings --schemadir $schemadir set org.gnome.shell.extensions.gnomenu   use-panel-menu-label  'true'


}



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo


#--------------------------------------------------------------------------------
# open gnome-software with search on gnome shell extension "gnomenu"
gnome-software --search=gnomenu


read -n 1 -s -r -p "When the extension is installed, Press any key to continue..."

#--------------------------------------------------------------------------------
# verify if click on install is realy done
 
path_extensions=~/.local/share/gnome-shell/extensions/gnomenu@panacier.gmail.com

# if the extension directory is present
if [ -d "$path_extensions" ]; then

   setup_gnomenu_extension

   # disable gnome-shell-extension app-menu
   /usr/bin/gnome-shell-extension-tool -d 'apps-menu@gnome-shell-extensions.gcampax.github.com'  && true

   # enable  gnome-shell-extension gnomenu
   /usr/bin/gnome-shell-extension-tool -e 'gnomenu@panacier.gmail.com'                           && true

else

   echo "error: you must click on install on the extension, and quit gnome-software"

fi


exit
