#!/bin/bash
# define the same randomized macaddress for eatch wifi spot
# source: https://fedoramagazine.org/randomize-mac-address-nm/


# Run in debug mode
#set -x

#trap  stop_on_error ERR

set -e

# source libraries
source libs/all;


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo


# define the same randomized macaddress for eatch wifi spot
if [ -e /etc/NetworkManager/conf.d/00-macrandomize.conf ]; then
   echo "/etc/NetworkManager/conf.d/00-macrandomize.conf already present, so we do nothing" 
else
   echo "[device]"                                      |   tee -a   /etc/NetworkManager/conf.d/00-macrandomize.conf
   echo "wifi.scan-rand-mac-address=yes"                |   tee -a   /etc/NetworkManager/conf.d/00-macrandomize.conf
   echo ""                                              |   tee -a   /etc/NetworkManager/conf.d/00-macrandomize.conf
   echo "[connection]"                                  |   tee -a   /etc/NetworkManager/conf.d/00-macrandomize.conf
   echo "wifi.cloned-mac-address=stable"                |   tee -a   /etc/NetworkManager/conf.d/00-macrandomize.conf
   echo "ethernet.cloned-mac-address=stable"            |   tee -a   /etc/NetworkManager/conf.d/00-macrandomize.conf
   echo "connection.stable-id=\${CONNECTION}/\${BOOT}"  |   tee -a   /etc/NetworkManager/conf.d/00-macrandomize.conf


   /usr/bin/systemctl restart NetworkManager

fi


# to get the uuid of a spot wifi
# nmcli c | grep wifi


# to get informations for a uuid 
# nmcli c show <uuid>


# to set a mac address permanent (whithout randomization of the macaddress)
# nmcli c modify <uuid> 802-11-wireless.cloned-mac-address permanent


# to set a mac address random (with randomization of the macaddress)
# nmcli c modify <uuid> 802-11-wireless.cloned-mac-address random


exit
