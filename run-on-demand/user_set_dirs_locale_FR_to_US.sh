#!/bin/bash
################################################################################
# This program is to use only after a Debian installation in French language
################################################################################
# It renames french directory in english and move files from old to new directories
#   - rename   "Bureau"          to  "Desktop"
#   - rename   "Images"          to  "Pictures"
#   - rename   "Modèles"         to  "Templates"
#   - rename   "Musique"         to  "Music"
#   - rename   "Téléchargements" to  "Downloads"
#   - rename   "Vidéos"          to  "Videos"
################################################################################



# Run in debug mode
#set -x


#trap  stop_on_error ERR

set -e

# source libraries
source libs/all




################################################################################
# 
################################################################################
move_and_remove_old_directory () {

local old_directory=$1
local new_directory=$2
local path_to_old_directory="$HOME/$old_directory"
local path_to_new_directory="$HOME/$new_directory"

# check if the old_directory exist
if [ -d "$path_to_old_directory" ]; then

   # check if the new_directory exist
   if [ -d "$path_to_new_directory" ]; then

      # if old_directory not empty 
      if [ -z "$(find $HOME -name $old_directory -type d -empty | xargs)" ]; then  
         # the old directory is empty, so we remove it
         echo "mv $path_to_old_directory/* $path_to_new_directory/   &&   rmdir $path_to_old_directory"
         mv $path_to_old_directory/* $path_to_new_directory/   &&   rmdir $path_to_old_directory
      else
         echo "rmdir $path_to_old_directory"
         rmdir $path_to_old_directory 
      fi

   else
      echo "$path_to_new_directory does not exist, so we do nothing..."
   fi

else
   echo "$path_to_old_directory does not exist, so we do nothing..."
fi

}



################################################################################
# creat new english directories and move files from old to new directories
################################################################################
user_dirs_locale_FR_to_US () {


echo 'en_US' > ~/.config/user-dirs.locale
LANG=C xdg-user-dirs-update --force
rm ~/.config/user-dirs.locale


move_and_remove_old_directory "Bureau"          "Desktop"
move_and_remove_old_directory "Images"          "Pictures"
move_and_remove_old_directory "Modèles"          "Templates"
move_and_remove_old_directory "Musique"         "Music"
move_and_remove_old_directory "Téléchargements" "Downloads"
move_and_remove_old_directory "Vidéos"          "Videos"




}



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo


# rename french directory in english 
#    "Bureau"          to  "Desktop"
#    "Images"          to  "Pictures"
#    "Modèles"         to  "Templates"
#    "Musique"         to  "Music"
#    "Téléchargements" to  "Downloads"
#    "Vidéos"          to  "Videos"
user_dirs_locale_FR_to_US                                                                                            >>./tmp/logfile 2>&1






exit
