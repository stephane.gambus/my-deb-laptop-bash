#!/bin/bash
################################################################################
# verif.sh : check files
################################################################################


# Run in debug mode
#set -x


trap  stop_on_error ERR

# source libraries
source libs/all




################################################################################
# 
################################################################################
apt_secure_check () {

local state_check=0   # state of the check
local https_in_sources_list="0"
local tor_in_sources_list="0"

if [ "$(cat /etc/apt/sources.list | grep "^deb" | grep "https" |wc -l)" != "0" ];     then https_in_sources_list="1" ; fi
if [ "$(cat /etc/apt/sources.list | grep "^deb" | grep " tor+http" |wc -l)" != "0" ]; then tor_in_sources_list="1"   ; fi

if [ "$https_in_sources_list" == "1" ]; then state_check="1"; fi
if [ "$tor_in_sources_list" == "1" ]; then state_check="1"; fi

echo_result_on_console "Check root_setup_apt_secure.sh" "$state_check"


}



################################################################################
# docker check
################################################################################
docker_check () {

local state_check=0   # state of the check
local the_user=$(get_user)
local state=0
local package="docker.io"
local docker_group="0"


state="$(dpkg --get-selections | grep "^$package" | wc -l)"
docker_group="$(groups $the_user | grep docker | wc -l)"


if [ "$state" == "1" ] && [ "$docker_group" == "1" ]; then state_check=1; fi

echo_result_on_console "Check root_setup_docker.sh" "$state_check"


}



################################################################################
# wifi_macaddress_randomize_check
################################################################################
wifi_macaddress_randomize_check () { 

local state_check=0   # state of the check

if [ -e /etc/NetworkManager/conf.d/00-macrandomize.conf ]; then

   if [ "$(cat /etc/NetworkManager/conf.d/00-macrandomize.conf | wc -l)" -eq 7 ]; then  
      state_check=1 
   fi

fi


echo_result_on_console "Check root_setup_wifi_macadress_randomize.sh" "$state_check"


}




################################################################################
# 
################################################################################
grub_system_secure_check () {

local state_check=0   # state of the check
local target=""
local line=""


state_check=0
target="mds=full,nosmt"
line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |grep "$target" | sed 's/\"//g' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result_on_console "Check root_setup_grub_system_secure.sh : $target" "$state_check"


state_check=0
target="slub_debug=P"
line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |grep "$target" | sed 's/\"//g' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result_on_console "Check root_setup_grub_system_secure.sh : $target" "$state_check"


state_check=0
target="page_poison=1"
line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |grep "$target" | sed 's/\"//g' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result_on_console "Check root_setup_grub_system_secure.sh : $target" "$state_check"


state_check=0
target="slab_nomerge"
line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |grep "$target" | sed 's/\"//g' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result_on_console "Check root_setup_grub_system_secure.sh : $target" "$state_check"


state_check=0
target="pti=on"
line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |grep "$target" | sed 's/\"//g' |wc -l)
if [ "$line" == "1" ]; then state_check=1 ; fi
echo_result_on_console "Check root_setup_grub_system_secure.sh : $target" "$state_check"



}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################



controls_with_sudo 


apt_secure_check

docker_check 

grub_system_secure_check

wifi_macaddress_randomize_check



exit 0
