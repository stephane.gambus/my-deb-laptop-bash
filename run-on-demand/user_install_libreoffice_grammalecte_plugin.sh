#!/bin/bash
################################################################################
# This program is to use only after a Debian installation in French language
################################################################################
# It install libreoffice grammalecte plugin
################################################################################



# Run in debug mode
#set -x


#trap  stop_on_error ERR

set -e

# source libraries
source libs/all






################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo

plugin_name="Grammalecte-fr-v1.4.0.oxt"
the_source='https://grammalecte.net/grammalecte/oxt/'$plugin_name
the_target='./tmp/'$plugin_name

# ---------------- Download The plugin file -----------------------

# if the file already exist, we remove it
if [ -e "$the_target" ]; then
   /usr/bin/bash -c 'rm '$the_target
fi

# download the plugin file
wget -O $the_target $the_source


# ---------------- check the plugin file checksum -----------------

sha512sum_target="acf96cc6fdaf68ad84156ca1299fd449132793bf027ddc7766c5faf55815525767993f7ec885e657c60a4a5016578d90f9ee096786f319d12d0239eac7b3ea4e"
sha512sum_source=$(sha512sum $the_target | awk '{ print $1 }')

if [ "$sha512sum_source" != "$sha512sum_target" ]; then
   echo "warning: the sha512sum signature has changed, so we exit"
   exit 1
else
   echo "Check sha512sum "$target" ...... : OK"
fi


# ---------------- install the plugin   ---------------------------

/usr/bin/unopkg add $the_target




exit
