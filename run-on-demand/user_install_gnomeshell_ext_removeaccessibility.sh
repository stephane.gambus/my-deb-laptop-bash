#!/bin/bash
# user_install_some_external_gnome_shell_extentions.sh 
# from https://extensions.gnome.org/extension/

# Run in debug mode
#set -x

#trap  stop_on_error ERR

set -e

# source libraries
source libs/all;



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo


#--------------------------------------------------------------------------------
# open gnome-software with search on gnome shell extension "gnomenu"
gnome-software --search="remove accessibility"


read -n 1 -s -r -p "When the extension is installed, Press any key to continue..."


#--------------------------------------------------------------------------------
# verify if click on install is realy done
 
path_extensions=~/.local/share/gnome-shell/extensions/removeaccesibility@lomegor

# if the extension directory is present
if [ -d "$path_extensions" ]; then

   # enable  gnome-shell-extension gnomenu
   /usr/bin/gnome-shell-extension-tool -e 'removeaccesibility@lomegor'                           && true     

else

   echo "error: you must click on install on the extension, and quit gnome-software"

fi


exit
