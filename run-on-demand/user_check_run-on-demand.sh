#!/bin/bash
################################################################################
# verif.sh : check files
################################################################################


# Run in debug mode
#set -x


trap  stop_on_error ERR

# source libraries
source libs/all



################################################################################
# check if $HOME/.config/user-dirs.locale is realy removed
################################################################################
user_dir_local_check () {

local state_check=0   # state of the check
local french_lang=0

french_lang=$(locale | grep "LANG=" | grep "fr_FR" | wc -l)

# if the system language is french
if [ $french_lang="1" ];  then

   # if the english directories are present
   if [ -d $HOME/Desktop ] && [ -d $HOME/Pictures ] && [ -d $HOME/Templates ] && [ -d $HOME/Music ] && [ -d $HOME/Downloads ] && [ -d $HOME/Videos ]; then

	    
      # if the french directories are not present
      if [ ! -d $HOME/Bureau ] && [ ! -d $HOME/Images ] && [ ! -d $HOME/Modèles ] && [ ! -d $HOME/Musique ] && [ ! -d $HOME/Téléchargements ] && [ ! -d $HOME/Vidéos ]; then

          if [ ! -f $HOME/.config/user-dirs.locale ];  then  state_check=1 ; fi

      fi
   fi 


fi

echo_result_on_console "Check user_set_dirs_locale_FR_to_US.sh"  "$state_check"


}



################################################################################
# grammalecte-fr_libreoffice_plugin_check
################################################################################
grammalecte-fr_libreoffice_plugin_check () {

local state_check=0   # state of the check

local target=""
local line=""

line=$(unopkg list | grep "Identifier: French.linguistic.resources.from.Dicollecte.by.OlivierR" | wc -l)
target="1"


if [ "$line" == "$target" ]; then state_check=1 ; fi

echo_result_on_console "Check user_install_libreoffice_grammalecte_plugin.sh"  "$state_check"

}



################################################################################
# grammalecte-fr_firefox_extension_check
################################################################################
grammalecte-fr_firefox_extension_check () {

local state_check=0   # state of the check

local target=""
local line=""

line=$(cat ~/.mozilla/firefox/*.default/addons.json | python -c 'import json,sys;obj=json.load(sys.stdin);
for (i, x) in enumerate(obj["addons"]):  print x["name"]' | uniq |grep "Grammalecte" |wc -l)
target="1"

if [ "$line" == "$target" ]; then state_check=1 ; fi

echo_result_on_console "Check user_install_firefox_grammalecte_extension.sh"  "$state_check"


}



################################################################################
#
################################################################################
installed_gnome_shell_extension_check () {


local state_check=0   # state of the check
local target="0"
local line=""
local path_extensions=~/.local/share/gnome-shell/extensions/



# -- Check: gnomenu --
state_check=0   
local target="1"
local line="$(ls "$path_extensions" | grep "gnomenu@panacier.gmail.com" | wc -l)"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result_on_console "Check user_install_gnomeshell_ext_gnomenu.sh"  "$state_check"


# -- check: No Topleft Hot Corner --
state_check=0   
local target="1"
local line="$(ls "$path_extensions" | grep "nohotcorner@azuri.free.fr" | wc -l)"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result_on_console "Check user_install_gnomeshell_ext_notoplefthotcorner.sh"  "$state_check"


# -- check: remove accessibility --
state_check=0   
local target="1"
local line="$(ls "$path_extensions" | grep "removeaccesibility@lomegor" | wc -l)"

if [ "$line" == "$target" ]; then state_check=1 ; fi
echo_result_on_console "Check user_install_gnomeshell_ext_removeaccessibility.sh"  "$state_check"


}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


#user=$(whoami)


controls_without_sudo 


user_dir_local_check

grammalecte-fr_libreoffice_plugin_check

grammalecte-fr_firefox_extension_check

installed_gnome_shell_extension_check


exit 0
