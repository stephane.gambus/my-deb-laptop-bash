#!/bin/bash
################################################################################
# This program is to use only after a Debian installation in French language
################################################################################
# It install firefox grammalecte extension
################################################################################



# Run in debug mode
#set -x


#trap  stop_on_error ERR

set -e

# source libraries
source libs/all






################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo

plugin_name="Grammalecte-fr-latest.xpi"
the_source='https://addons.mozilla.org/firefox/downloads/file/2986443'
the_target='./tmp/'$plugin_name

# ---------------- Download The plugin file -----------------------

# if the file already exist, we remove it
if [ -e "$the_target" ]; then
   /usr/bin/bash -c 'rm '$the_target
fi

# download the plugin file
wget -O $the_target $the_source


# ---------------- check the plugin file checksum -----------------

sha512sum_target="ecba7b7e0c862b419a245b380b78c6e992e2646da1f95b6569ed0bde8f4ccc810ff4b2ea1fb14cb070baf38fb9834556b1ff0b11a1f04f6cec0573047788fdec"
sha512sum_source=$(sha512sum $the_target | awk '{ print $1 }')

if [ "$sha512sum_source" != "$sha512sum_target" ]; then
   echo "warning: the sha512sum signature has changed, so we exit"
   exit 1
else
   echo "Check sha512sum "$target" ...... : OK"
fi


# ---------------- install the plugin   ---------------------------

firefox $the_target




exit
