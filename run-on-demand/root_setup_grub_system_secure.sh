#!/bin/bash
# root_setup_grub.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;




################################################################################
# add kernel boot param in grub for hardening debian
# source: https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings#kernel_command_line_options
# source: https://www.youtube.com/watch?v=N-pvLMHtRSA
################################################################################
add_kernel_boot_param_in_grub () {

# remove parameters if they are already presents
sed -i 's/ mds=full,nosmt//'  /etc/default/grub
sed -i 's/ slub_debug=P//'    /etc/default/grub
sed -i 's/ page_poison=1//'   /etc/default/grub
sed -i 's/ slab_nomerge//'    /etc/default/grub
sed -i 's/ pti=on//'          /etc/default/grub


local line=$(cat /etc/default/grub | grep '^GRUB_CMDLINE_LINUX_DEFAULT=' |sed 's/GRUB_CMDLINE_LINUX_DEFAULT=//g' | sed 's/\"//g')

# -----------------------------------------------------------------------------
# enable all available mitigations for the MDS vulnerability + disable smt (hyperthreading)
# https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html
# cat /sys/devices/system/cpu/vulnerabilities/mds
line=$line" mds=full,nosmt"


# -----------------------------------------------------------------------------
# Enable slub/slab allocator free poisoning (requires CONFIG_SLUB_DEBUG=y above).
line=$line" slub_debug=P"


# -----------------------------------------------------------------------------
# Enable buddy allocator free poisoning (requires CONFIG_PAGE_POISONING=y above).
line=$line" page_poison=1"


# -----------------------------------------------------------------------------
# Disable slab merging (makes many heap overflow attacks more difficult).
line=$line" slab_nomerge"


# -----------------------------------------------------------------------------
# Always enable Kernel Page Table Isolation, even if the CPU claims it is safe from Meltdown.
line=$line" pti=on"



local SEARCH='^GRUB_CMDLINE_LINUX_DEFAULT=.*'
local REPLACE='GRUB_CMDLINE_LINUX_DEFAULT=\"'$line'\"'
local FILEPATH="/etc/default/grub"
sed -i "s;$SEARCH;$REPLACE;" $FILEPATH



}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo


add_kernel_boot_param_in_grub


# update grub
/usr/sbin/update-grub                                                                      



exit
