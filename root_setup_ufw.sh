#!/bin/bash


# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all




################################################################################
# setup ufw firewall
################################################################################
setup_ufw_firewall () {


   # -------------------------
   # Initialization
   # -------------------------
   #ufw --force reset           # reset all firewall parameters
   ufw enable                  # enable firewall


   # -------------------------
   # Default Policies
   # -------------------------
   ufw default deny incoming    # default incoming policy
   ufw default allow outgoing   # default outgoing policy



   # -------------------------
   # Allow incoming rules
   # -------------------------
   ufw allow in proto udp to   224.0.0.0/4   # allow udp multicast packets out
   ufw allow in proto udp from 224.0.0.0/4   # allow udp multicast packets in
   ufw allow in 9001/tcp                     # allow in tor 


}



################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_with_sudo


packagename="ufw"

# check if the package is installed
state=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}\n' $packagename || true)

if [ "$state" == "installed" ]; then

   setup_ufw_firewall

else
   echo "the package $packagename is not installed, so we do nothing"

fi




exit
