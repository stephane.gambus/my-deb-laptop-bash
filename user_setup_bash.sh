#!/bin/bash
# all_setup_bashrc_completion.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR


# source libraries
source libs/all;


################################################################################
# uncomment things in bashrc
################################################################################
uncomment_things_in_bashrc () {


   file_path="$HOME/.bashrc"

   if [ -e "$file_path" ]; then

      sed -i 's/#alias /alias /g'                                   $file_path && echo "Uncomment  alias:                                   OK"
      sed -i 's/# alias /alias /g'                                  $file_path && echo "Uncomment  alias:                                   OK"

      sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/'    $file_path && echo "Uncomment  force_color_prompt:                      OK"
      sed -i 's/grep --color=auto/grep --color=always/'             $file_path && echo "Change     grep --color=   from 'auto' to 'always': OK"

   else
      echo "error: $file_path does not exist."
   fi


}


################################################################################
# add completion in bashrc
################################################################################
add_completion_in_bashrc () {


   path_to_file="$HOME/.bashrc"

   if [ -e "$path_to_file" ]; then

      if [ "$(cat $path_to_file | grep '^if ! shopt -oq posix; then' | wc -l)" == 0 ]; then

         echo ""                                                                  | tee -a     $path_to_file 
         echo "if ! shopt -oq posix; then"                                        | tee -a     $path_to_file
         echo "   if [ -f /usr/share/bash-completion/bash_completion ]; then"     | tee -a     $path_to_file
         echo "     . /usr/share/bash-completion/bash_completion"                 | tee -a     $path_to_file 
         echo "   elif [ -f /etc/bash_completion ]; then"                         | tee -a     $path_to_file
         echo "     . /etc/bash_completion"                                       | tee -a     $path_to_file
         echo "   fi"                                                             | tee -a     $path_to_file
         echo "fi"                                                                | tee -a     $path_to_file
      else
         echo "bash completion lines are already present in $path_to_file"
      fi 

   else
      echo "File $path_to_file does not exist, so we do nothing..."
   fi


}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_without_sudo


uncomment_things_in_bashrc


add_completion_in_bashrc



exit
