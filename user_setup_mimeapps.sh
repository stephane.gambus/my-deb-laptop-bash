#!/bin/bash
# user_setup_xdg_mime.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all; 


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo

cp -f ./lists/mimeapps.list $HOME/.config/mimeapps.list


exit
