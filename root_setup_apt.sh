#!/bin/bash
# root_setup_apt.sh 

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
# comment the "deb cdrom" line in sources.list       
################################################################################
comment_cdrom () {

   sed -i '/^deb cdrom:/ s/^/# /' /etc/apt/sources.list                           || echo "problem on sed apt-cdrom"    


}


################################################################################
# workaround before using  apt-add-repository
################################################################################
workaround () {

   # for the case of the debian install was made with the unofficial version with firmwares non-free:
   # we have to remove "contrib" and "non-free" before running apt-add-repository because when contrib and non-free are already present, but only on one line.
   # the command /usr/bin/apt-add-repository -s does not add contrib non-free on the other lines 
   sed -i 's/ contrib//'  /etc/apt/sources.list                                  || echo "problem on sed contrib"    
   sed -i 's/ non-free//' /etc/apt/sources.list                                  || echo "problem on sed non-free"    


}


################################################################################
# add "contrib" in sources.list
################################################################################
add_contrib () {

   # add contrib
   /usr/bin/apt-add-repository -s contrib


}


################################################################################
# add "non-free" in sources.list
################################################################################
add_non_free () {

   # add non-free
   /usr/bin/apt-add-repository -s non-free


}


################################################################################
# add backports repository
################################################################################
add_backports () {

   # add backports
   if [ "$(cat /etc/apt/sources.list | grep "^deb" | grep " buster-backports " | wc -l)" -eq 0 ]; then

      #  /usr/bin/apt-add-repository -s 'deb http://deb.debian.org/debian buster-backports main contrib non-free'

      echo ""                                                                              | tee -a  /etc/apt/sources.list
      echo "deb http://deb.debian.org/debian buster-backports main contrib non-free"       | tee -a  /etc/apt/sources.list
      echo "deb-src http://deb.debian.org/debian buster-backports main contrib non-free"   | tee -a  /etc/apt/sources.list

   fi


}



################################################################################
# 
################################################################################
enable_seccomp_sandbox () {


echo 'apt::sandbox::seccomp "true";' > /etc/apt/apt.conf.d/999seccomp


}


################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################




controls_with_sudo


{
   trap  stop_on_error ERR


   echo_for_whiptail "0" "setup /etc/apt/sources.list"

   comment_cdrom                                                                               >>./tmp/logfile 2>&1
   echo_for_whiptail "20" "setup /etc/apt/sources.list"

   workaround                                                                                  >>./tmp/logfile 2>&1
   echo_for_whiptail "30" "setup /etc/apt/sources.list"

   add_contrib                                                                                 >>./tmp/logfile 2>&1
   echo_for_whiptail "40" "setup /etc/apt/sources.list"

   add_non_free                                                                                >>./tmp/logfile 2>&1
   echo_for_whiptail "50" "setup /etc/apt/sources.list"

   add_backports                                                                               >>./tmp/logfile 2>&1
   echo_for_whiptail "60" "setup /etc/apt/sources.list"

   enable_seccomp_sandbox                                                                      >>./tmp/logfile 2>&1
   echo_for_whiptail "70" "create /etc/apt/apt.conf.d/999seccomp"

   apt -q update                                                                               >>./tmp/logfile 2>&1


   echo_for_whiptail "100" "apt upgrade"
   sleep 1

}    |   whiptail --title "apt" --gauge "Configuration " 6 60 0


# apt upgrade
/usr/bin/debconf-apt-progress -- /usr/bin/apt -q -y upgrade



exit
