#!/bin/bash
# root_setup_network_manager.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
# Create /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf" 
# source: https://gist.github.com/jcberthon/ea8cfe278998968ba7c5a95344bc8b55
################################################################################
wifi_powersave_config() {

if [ -e "/etc/NetworkManager/conf.d/default-wifi-powersave-on.conf" ]; then
   echo "/etc/NetworkManager/conf.d/default-wifi-powersave-on.conf already exist, so do nothing"
else
   echo "[connection]"             | tee -a  /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf
   echo "wifi.powersave = 3"       | tee -a  /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf
fi


}




################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################

controls_with_sudo


# Create /etc/NetworkManager/conf.d/default-wifi-powersave-on.conf" 
wifi_powersave_config





exit
