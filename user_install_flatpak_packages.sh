#!/bin/bash
# user_install_flatpak_packages.sh

# Run in debug mode
#set -x

trap  stop_on_error ERR

# source libraries
source libs/all;



################################################################################
# check parameters
################################################################################
check_parameters () {

local list_file="$1"



# Check $listfile existance. if not, we exit
if ! [ -e "$list_file" ]; then 
   echo "user_flatpak_package_installation.sh:   first parameter need to be an existing file"
   exit 1
fi


}



################################################################################
# flatpak_update
################################################################################
flatpak_update () {

   {
      echo_for_whiptail "0" "flatpak update"
      /usr/bin/flatpak -y update           2>./tmp/logfile
      echo_for_whiptail "100" "flatpak update"
      sleep 1

   }  | whiptail --title "Progress" --gauge "flatpak update" 6 60 0


}




################################################################################
################################################################################
################################################################################
################################################################################
###############################              ###################################
###############################     main     ###################################
###############################              ###################################
################################################################################
################################################################################
################################################################################


controls_without_sudo


filepath="./lists/flatpack_flathub.list" 
text="flatpak"


# check parameters
check_parameters "$filepath" "$text"


# get nomber of lines of $filepath
nbline=$(wc -l $filepath | awk '{ print $1 }')

# calculate on percent value
onepercent=$(echo 100 / $nbline | bc -l)


# update before installation
flatpak_update


# installation 
{

    i=0
    while read -r line; do

        i=$(( i + 1 ))

        echo_for_whiptail "$(echo "$onepercent * $i" |bc | cut -d . -f 1)" "$text: Installing $line"

	# Download and install apps in file flatpack_flathub.list
        /usr/bin/flatpak -y install flathub  $line   2>/dev/null

    done < <(cat $filepath | awk '{ print $1 }')



    echo_for_whiptail "100" "Installed"
    sleep 1


}  | whiptail --title "Progress" --gauge "Installing" 6 60 0





exit
